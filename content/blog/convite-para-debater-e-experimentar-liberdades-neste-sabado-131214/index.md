---
kind: article
created_at: 2014-12-09
title: Convite para debater e experimentar liberdades neste sábado 13/12/14  
image_file: banner-debate-liberdades.png
image_alt: Liberdades
author: Paulo Henrique de Lima Santana
short_description: |
  A Rede Livre promove neste sábado um encontro das liberdades onde vamos discutir e viver experiências em Software Livre, Cultura Livre, Mídia democrática, e Permacultura/Agroecologia/Cidade para pessoas, entendendo que a liberdade representa a conexão entre estes temas.
---

A [Rede Livre](http://www.redelivre.org.br/) promove neste sábado um encontro das liberdades onde vamos discutir e viver experiências em Software Livre, Cultura Livre, Mídia democrática, e Permacultura/Agroecologia/Cidade para pessoas, entendendo que a liberdade representa a conexão entre estes temas.

Acolhida: 10h

## Trilhas e programação:

### Software livre

10h30-11h30

Vamos refletir sobre a importância do conhecimento livre para o código dos softwares que utilizamos, da potência proporcionada pelo desenvolvimento colaborativo, os novos arranjos econômicos e as potencialidades para movimentos e organizações sociais.

Objetivo: Refletir sobre o tema e conectar desenvolvedores e usuários a partir da apresentação de experiências e soluções.

Facilitador: Paulo Henrique Santana

### Permacultura/Agroecologia/Cidade para pessoas

11h30-12h30

O objetivo é conectar pessoas e organizações para trocar experiências sobre o desenvolvimento destes, entre outros movimentos afins, buscando traçar ações e estratégias conjuntas para potencialização.

Facilitador: Fabio Henrique Nunes

### Mídia Democrática

14h30-15h30

Num momento em que os movimentos sociais vão às ruas pedindo e lutando por reformas, a bandeira da democratização da comunicação ergue-se como pauta fundamental e imprescindível para a construção de uma sociedade verdadeiramente democrática e livre.

Objetivo: analisar a atual política de comunicação do país desde a realização da Conferência Nacional da Comunicação, que nesta semana completa 5 anos, apontando conquistas e futuras lutas.

Facilitadora: Ana Paula Salamon

### Cultura Livre

15h30-16h30

A proposta é provocar artistas e comunidade a refletir sobre as mudanças de paradigma que a tecnologia trouxe também para a Cultura.

Objetivo: Compreender as novas formas de arranjos culturais.

Facilitadora: Thalita Sejanes

### E mais:

Install Fest: traga seu notebook/computador para que voluntários ajudem a instalar o sistema operacional GNU/Linux completo ou Softwares Livres em outros sistemas operacionais como Windows.

Almoço: Participe dos debates e atividades e não esqueça de trazer sua comida e bebida para o almoço no local! Temos uma churrasqueira para onívoros e vegetarianos :-)

Obs: o encontro é aberto a todos, não precisa nenhum tipo de inscrição. É só vir e participar.

* Local: Ethymos Soluções em Web
* Endereço: Rua Itupava, 1.299 - sala 312 - Curitiba
* Organização: Rede Livre

![Debate](/images/debate-liberdades.png)
