---
kind: article
created_at: 2014-07-29
title: Grade de programação do FTSL e SFD em Curitiba disponível    
image_file: banner-semana-2014.png
image_alt: Grade
author: Paulo Henrique de Lima Santana
short_description: |
  Foi lançada hoje a grade de programação com os horários das oficinas, palestras, painéis e encontros comunitários que acontecerão durante o 6º Fórum de Tecnologia em Software Livre (FTSL) e o 10º Software Freedom Day (SFD) em Curitiba.
---

Foi lançada hoje a grade de programação com os horários das oficinas, palestras, painéis e encontros comunitários que acontecerão durante o **6º Fórum de Tecnologia em Software Livre (FTSL) e o 10º Software Freedom Day (SFD)** em Curitiba de 18 a 20 de setembro no Campus central Universidade Tecnológica Federal do Paraná (UTFPR).

As atividades ficaram distribuídas assim:

Nos dias 18 e 19 (dias do FTSL) pela manhã acontecerão 4 oficinas em paralelo nos laboratórios com 3 horas de duração cada. No período da tarde acontecerão as palestras, painéis e encontros comunitários alocados no auditório, mini-auditório e salas (não teremos oficinas a tarde). E no período da noite continuam as palestras, painéis e encontros comunitários, e voltam as 4 oficinas nos laboratórios até às 21h.

Esta é a primeira vez que o FTSL terá atividades a noite. A organização busca assim prestigiar aqueles participantes que trabalham em horário comercial e não tem a oportunidade de participar das atividades durante os períodos da manhã e tarde.

No dia 20 (dia do SFD) todas as atividades (palestras, painéis, encontros comunitários e oficinas) acontecerão de manhã e a tarde, começando às 9h até o encerramento às 18h.

Consulte a grade [aqui](http://papers.ftsl.org.br/6/pub).

Ao todo serão 24 oficinas, 6 painéis, 7 encontros comunitários e mais de 80 palestras, Tudo isso de graça!

Aproveite e faça já a sua inscrição gratuita: www.ftsl.org.br/inscricao

Comissão Organizadora

6º Fórum de Tecnologia em Software Livre / 10º Software Freedom Day

18 a 20/09/2014 - Campus da UTFPR

Curitiba - PR
