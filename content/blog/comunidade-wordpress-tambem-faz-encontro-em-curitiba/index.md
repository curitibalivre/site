---
kind: article
created_at: 2015-07-07
title: Comunidade WordPress também faz encontro em Curitiba  
image_file: banner-meetup-wordpress-curitiba.png
image_alt: Meetup WordPress
author: Paulo Henrique de Lima Santana
short_description: |
  Como já divulgamos em outra notícia, as comunidades de Python e Ruby farão encontros nos próximos dias em Curitiba, nos dias 29 de julho e 08 de agosto respectivamente.
---

Como já divulgamos em [outra notícia](http://curitibalivre.org.br/blog/comunidades-grupy-pr-e-guru-pr-fazem-encontros-em-curitiba), as comunidades de Python e Ruby farão encontros nos próximos dias em Curitiba, nos dias 29 de julho e 08 de agosto respectivamente.

Agora é a vez da comunidade WordPress divulgar o **1º MeetUp WordPress Curitiba 2015** que eles estão organizando no dia 05 de agosto, das 19:00h às 22:00h na [Aldeia Coworking](https://aldeia.cc).
 
A Aldeia Coworking fica na Rua Marechal Deodoro, 262 - 1º andar - Galeria Suissa, Curitiba.

Mais informações: <http://www.meetup.com/pt/wpcuritiba/events/224130389>
