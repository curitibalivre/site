---
kind: article
created_at: 2013-09-26
title: SFD 2013 em Curitiba  
image_file: banner-sfd-2013.png
image_alt: SFD
author: Paulo Henrique de Lima Santana
short_description: |
 Agradecemos a todos que colaboraram de alguma forma para a realização do Software Freeedom Day 2013 em Curitiba. 
---

Agradecemos a todos que colaboraram de alguma forma para a realização do Software Freeedom Day 2013 em Curitiba. Organizadores (aqueles que ajudaram antes e os que ajudaram apenas no dia), palestrantes, apoiadores, e pessoal em geral.

O evento foi um sucesso! Todos os palestrantes e instrutores estiveram presente e tivemos um pouco mais de 100 participantes entre inscritos e organizadores,  o que é um recorde entre os eventos que organizamos nos últimos anos em Curitiba.

Não sei se é possível passar muito mais essa quantidade de pessoas presentes aqui em Curitiba. Acredito que o que chama muita gente são a oficinas. Sempre tive a impressão que o pessoal quer mesmo é por a mão-na-massa em coisas práticas do que ficar apenas assistindo palestras. Mas organizar oficinas demanda muito mais trabalho da organização e principalmente uma boa estrutura de laboratórios e internet (coisa que sempre é complicado).

A novidade do SFD ficou por conta do espaço "Conhecendo o Software Livre" onde colocamos 6 banners explicando didaticamente o que é Software Livre e sua história, o que é GNU/Linux e suas Distribuições,  mostramos algumas ferramentas e aplicativos livres, alguns jogos e algumas comunidades existentes.

Veja as pessoas que contribuiram voluntariamente para a realização do SFD 2013 em Curitiba:

<http://softwarelivre.org/sfd2013-curitiba/blog/agradecimentos-as-pessoas-do-sfd-2013>

Confira os brindes doados por nossos parceiros para o sorteio entre os participantes: http://softwarelivre.org/sfd2013-curitiba/blog/brindes-para-sorteios-no-sfd-2013-em-curitiba

Fotos: 

<http://softwarelivre.org/sfd2013-curitiba/fotos>

![Foto](/images/sfd-2013-curitiba-001.jpg =600x)

![Foto](/images/sfd-2013-curitiba-124.jpg =600x)

