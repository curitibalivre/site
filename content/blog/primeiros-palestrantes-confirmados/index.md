---
kind: article
created_at: 2014-05-30
title: Primeiros palestrantes confirmados     
image_file: banner-semana-2014.png
image_alt: FTSL
author: Paulo Henrique de Lima Santana
short_description: |
 Veja abaixo os nossos primeiros palestrantes confirmados!
---

Veja abaixo os nossos primeiros palestrantes confirmados!

* **João Eriberto Mota Filho** é Oficial de Cavalaria do Exército Brasileiro. Gerente de Rede e de Segurança em Rede do Gabinete do Comandante do Exército. Professor da Pós-graduação em Perícia Digital na Universidade Católica de Brasília (UCB). Autor dos livros Linux & Seus Servidores (2000), Pequenas Redes com Microsoft Windows (2001), Descobrindo o Linux (3ª edição em 2012) e Análise de tráfego em redes TCP/IP (2013). Mantenedor Debian. Membro do time de Forense do Debian.

* **André Noel** é Bacharel e Mestrando em Ciência da Computação (UEM). É programador e usuário Linux desde 2002, membro oficial da Comunidade Ubuntu. Trabalhou por diversos anos com programação, por isso aproveitou a experiência na área, juntando com o fato de que nunca foi levado muito a sério, e hoje se dedica ao humor e programa para si próprio. Profundo conhecedor de métodos de desenvolvimento alternativos (gambiarras) e totalmente viciado em café. É o criador do site Vida de Programador, responsável por manipular os vetores que se transformam em tirinhas. Aproveitando-se de sua inigualável beleza, começou a se dedicar também a fazer um videolog. É casado com Raquel, pai do Mateus e da Gabrielle.

* **Júlio Cezar Neves** é engenheiro de produção da UFRJ, pós-graduado em informática pelo IBAM, Analista de Suporte de Sistemas desde 1969. Ex-diretor de Informática do IplanRIO, órgão de TI da Prefeitura Municipal do Rio de Janeiro. Trabalha com Unix desde 1980, quando fez parte da equipe que desenvolveu o SOX, sistema operacional Unix-Like, da Cobra Computadores. Professor da Universitário e autor dos livros "Bombando o Shell" e "Programação Shell - Linux", as melhores publicações nacionais de programação shell, já em sua 9ª edição. Além disto, mantém também este site, que oferece um material completo sobre programação Shell.
