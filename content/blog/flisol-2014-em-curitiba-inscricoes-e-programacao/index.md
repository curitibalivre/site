---
kind: article
created_at: 2014-03-25
title: FLISOL 2014 em Curitiba - inscrições e programação   
image_file: banner-flisol-2014.png
image_alt: Flisol
author: Paulo Henrique de Lima Santana
short_description: |
 Estão abertas as inscrições gratuitas para o Festival Latino-americano de Instalação de Software Livre (FLISOL) que acontecerá no dia 26 de abril de 2014 (sábado) das 10h às 18h no Campus da PUC em Curitiba.
---

Estão abertas as inscrições **gratuitas** para o **Festival Latino-americano de Instalação de Software Livre (FLISOL)** que acontecerá no dia 26 de abril de 2014 (sábado) das 10h às 18h no Campus da PUC em Curitiba.

Também já disponível no site do evento a grade completa da programação de palestras e oficinas. Este ano teremos no FLISOL em Curitiba:

* Install Fest onde voluntários capacitados instalarão GNU/Linux nos computadores/notebooks dos visitantes durante todo o dia;
* 4 oficinas práticas distribuídas em 2 laboratórios;
* 11 palestras distribuídas em 2 salas, sendo que uma destas salas terá palestras focadas no público iniciante com temas básicos como o que é Software Livre, GNU/Linux, LibreOffice, Gimp, Inkscape, etc

Destacamos ainda a vinda de Maringá do nosso palestrante convidado André Noel  do site Vida de Programador (<http://vidadeprogramador.com.br>) que dará uma palestra sobre como ele criou e mantém o site usando ferramentas livres.

Resumindo:

* FLISOL 2014 em Curitiba
* Data: 26 de abril (sábado)
* Horário: 10h às 18h
* Local: PUC-PR - Rua Imaculada Conceição, 1155 - Prado Velho

Informações e inscrição:

<http://softwarelivre.org/flisol2014-curitiba>

Obs: a inscrição dará direito a certificado de participação.

Organização:

* Comunidade Curitiba Livre - <http://www.curitibalivre.org.br>
* Pontifícia Universidade Católica do Paraná - PUCPR - <http://www.pucpr.br>
