---
kind: article
created_at: 2014-04-27
title: Chamada de trabalhos para o FTSL e SFD em Curitiba   
image_file: banner-semana-2014.png
image_alt: FTSL
author: Paulo Henrique de Lima Santana
short_description: |
 A organização do VI Fórum de Tecnologia em Software Livre (FTSL) e do Software Freedom Day 2014 (SFD) em Curitiba - PR convoca a comunidade para contribuir com sua programação.
---

A organização do **VI Fórum de Tecnologia em Software Livre (FTSL)** e do **Software Freedom Day 2014 (SFD)** em Curitiba - PR convoca a comunidade para contribuir com sua programação. Estudantes de qualquer escola ou universidade, professores e/ou profissionais podem enviar suas propostas relacionadas com Software Livre nas categorias: palestra, oficina/minicurso e encontro de comunidade.

O FTSL acontecerá nos dias 18 e 19 de setembro e o SFD no dia 20 de setembro. As propostas enviadas são válidas para os dois eventos e eles acontecerão na Universidade Tecnológica Federal do Paraná (UTFPR) - Campus Curitiba - Sede Central.

Data limite para submissão: **30 de junho de 2014**.

Mais informações e formulário de submissão em:

<http://www.ftsl.org.br/chamada-de-trabalhos>

Dia 17 de maio teremos uma confraternização da Comunidade Curitiba Livre

![Banner](/images/banner-fb-ftsl-sfd.png)
