---
kind: article
created_at: 2014-07-15
title: Próxima reunião da Comunidade Curitiba Livre 19/07/14    
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
  O pessoal da Ethymos cedeu uma sala para realizarmos a nossa próxima reunião da Comunidade.
---

O pessoal da [Ethymos](http://ethymos.com.br/) cedeu uma sala para realizarmos a nossa próxima reunião da Comunidade.

Resumindo:

* Reunião da Comunidade Curitiba Livre
* Data: 19/07 (sábado)
* Horário: 9:15 às 12:00h
* Local: Ethymos
* Endereço: Rua Itupava, 1.299 - sala 312 - Curitiba

Sugestão de pauta:

* Discussão sobre a logo da Comunidade
* Organização dos próximos eventos de SL em Curitiba
* Feira de Cursos e Profissões da UFPR
* Debian Day
* Outros assuntos que surgirem

