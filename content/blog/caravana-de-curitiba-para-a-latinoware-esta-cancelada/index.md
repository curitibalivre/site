---
kind: article
created_at: 2015-09-28
title: Caravana de Curitiba para a Latinoware está cancelada 
image_file: banner-latinoware-2015.png
image_alt: Caravana Latinoware 2015
author: Paulo Henrique de Lima Santana
short_description: |
  A organização da Latinoware 2015 selecionou Curitiba para receber um dos ônibus patrocinados que serviriam para a caravana que levaria os participantes para o evento.
---

A organização da [Latinoware 2015](https://2015.latinoware.org) selecionou Curitiba para receber um dos ônibus patrocinados que serviriam para a caravana que levaria os participantes para o evento.

Uma das regras para a caravana é que  o ônibus deve ter no mínimo 35 passageiros inscritos na Latinoware. Mas infelizmente até esta data temos apenas 5 pessoas interessadas em ir na caravana, o que inviabiliza a saída do ônibus.

Por isso informamos que a caravana de Curitiba para a Latinoware está **cancelada**.

![Latinoware](/images/latinoware-2015.png) 
