---
kind: article
created_at: 2015-04-16
title: Festa de Lançamento do Debian 8 em Curitiba acontecerá durante o FLISOL
image_file: banner-festa-lancamento-debian-8.png
image_alt: Festa de Lançamento do Debian 8
author: Paulo Henrique de Lima Santana
short_description: |
  Foi anunciado para o dia 25 de abril o lançamento da versão 8.0 do Debian GNU/Linux (codinome "Jessie").
---

[Foi anunciado](https://lists.debian.org/debian-devel-announce/2015/03/msg00016.html) para o dia 25 de abril o lançamento da versão 8.0 do Debian GNU/Linux (codinome "Jessie").

Como neste mesmo dia acontecerá o [FLISOL - Festival Latino-americano de Instalação de Software Livre](http://www.softwarelivre.org/flisol2015-curitiba), a Comunidade Curitiba Livre irá comemorar a chegada da nova versão do Debian durante o evento.

O FLISOL acontecerá das 9:00 às 17:00h no Campus da PUCPR.  Durante todo o dia acontecerão diversas palestras e oficinas, e uma delas será sobre Debian.

* Título: Lançamento do Debian 8 - conheça as novidades da nova versão do Debian (Jessie).
* Descrição: No dia 25 de abril será lançada oficialmente a nova versão do Debian. Para celebrar esta data, vou falar sobre o que é e como funciona o Debian; Novidades do Debian 8 (Jessie); como colaborar com o Debian.
* Palestrante: Antonio Terceiro
* Currículo: Desenvolvedor, sócio da Colivre onde participei da criação, e participo do desenvolvimento, do Noosfero. Atualmente trabalho também no projeto de reformulação do Software Público Brasileiro como pesquisador bolsista do LAPPIS/UNB. Também sou membro do projeto Debian, onde participo do time de Ruby e mantenho o projeto de integração contínua do Debian.
* Horário da palestra: 14:00h

Os visitantes poderão aproveitar o Install Fest e levar o seu computador/notebook para instalar o Debian Jessie

Resumo:

* Quando: 25 de abril de 2015
* Horário: 9:00h às 17:00h
* Onde: PUCPR - Rua Imaculada Conceição, 1155 - Prado Velho (mapa)

![Cartaz](/images/festalancamentodebian8.jpg)
