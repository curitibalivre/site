---
kind: article
created_at: 2017-05-24
title: Divulgação do 19º Encontro Locaweb Curitiba
image_file: banner-19-locaweb.png
image_alt: Locaweb
author: Paulo Henrique de Lima Santana
short_description: |
  A Locaweb fez novamente uma parceria com a Comunidade Curitiba Live para oferecer um desconto de 25% na inscrição para o 19º edição do roadshow Encontro Locaweb em Curitiba, além dos 25% de desconto que já estão no site.
---

A [Locaweb](https://www.locaweb.com.br) fez novamente uma parceria com a **Comunidade Curitiba Live** para oferecer um desconto de 25% na inscrição para o **19º edição do roadshow Encontro Locaweb** em Curitiba, além dos 25% de desconto que já estão no site.

Acesse o [site](http://eventos.locaweb.com.br/proximos-eventos/19o-encontro-locaweb-curitiba) do evento e no momento da inscrição use o código **PRCCURITIBALIVRE** para receber o desconto e pagar R$ 100,00 até 26 de maio.


