---
kind: article
created_at: 2017-09-08
title: Software Freedom Day em Curitiba será no dia 16/09/2017
image_file: banner-sfd-2017.png
image_alt: SFD 2017
author: Paulo Henrique de Lima Santana
short_description: |
  Se você gosta e usa software livre, ou quer aprender mais sobre esse tema, não perca o Software Freedom Day no Jupter no dia 16 de setembro de 2017.
---

Se você gosta e usa software livre, ou quer aprender mais sobre esse tema, não perca o Software Freedom Day no Jupter no dia 16 de setembro de 2017.

<http://sfd.curitibalivre.org.br>

![Banner SFD 2017](/images/sfd-2017.png =600x)

