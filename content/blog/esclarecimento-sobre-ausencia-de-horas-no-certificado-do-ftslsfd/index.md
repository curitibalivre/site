---
kind: article
created_at: 2014-10-24
title: Esclarecimento sobre ausência de horas no certificado do FTSL/SFD  
image_file: banner-semana-2014.png
image_alt: Esclarecimento
author: Paulo Henrique de Lima Santana
short_description: |
  Os membros da Comunidade Curitiba Livre que participaram da organização da 1ª Semana de Software Livre de Curitiba (FTSL/SFD) vem por meio desta esclarecer:
---

Os membros da Comunidade Curitiba Livre que participaram da organização da [1ª Semana de Software Livre de Curitiba (FTSL/SFD)](http://www.ftsl.org.br/) vem por meio desta esclarecer:

* A organização do evento foi compartilhada entre membros da Comunidade Curitiba Livre, funcionários da Regional SERPRO de Curitiba e professores da UTFPR.

* Todas as decisões sempre foram discutidas entre os participantes da organização e quando havia um ponto divergente onde não era possível chegar a um consenso, cada grupo tinha direito a um voto, ou seja, eram três votos no total, sendo: um da Comunidade, um do Serpro, e um da UTFPR.

* Em todos os eventos organizados pelos membros da Comunidade Curitiba Livre nos últimos anos como o Education Freedom Day, FLISOL, Software Freedom Day, e Debian Day, quando houve a emissão de certificados, neles constavam a carga horária total afim de valer como comprovante para que os estudantes pudessem usá-las como horas formativas em seus cursos.

* Após o fim da 1ª Semana de Software Livre de Curitiba, a organização passou a discutir o modelo do certificado de participação do evento e os membros da Comunidade Curitiba Livre insistiram bastante para que nele constasse a carga horária total de 23 horas.

* Grandes eventos como [FISL - Fórum Internacional de Software Livre](http://www.fisl.org.br/), [Latinoware - Conferência Latino-americana de Software Livre](http://www.latinoware.org/) e [Campus Party Brasil](http://www.latinoware.org/) emitem seus certificados com a carga horária total mesmo sem verificar se os participantes realmente estiverem durante todas as horas no evento. Portanto, essa é uma prática comum que em nada desabona ou desqualifica estes importantes eventos.

* Infelizmente os representantes do SERPRO e da UTFPR não concordaram com a colocação das horas no certificado e fomos voto vencido por 2 a 1. Por isso o certificado está sendo emitido sem a carga horária total.

* Gostaríamos de ressaltar que respeitamos a decisão da maioria (SERPRO e UTFPR) mas não concordamos por entender o quanto é importante para os estudantes a colocação dessas horas.

* Aqueles que se sentirem prejudicados e precisarem das horas no certificado, podem enviar e-mail para: contato@ftsl.org.br

Membros da Comunidade Curitiba livre que participaram da organização da 1ª Semana de Software Livre de Curitiba (FTSL/SFD):

* Adriana Cássia da Costa
* Cleber Ianes
* Daniel Lenharo de Souza
* Eduardo da C. V. Quagliato
* Paulo Henrique de Lima Santana
* Samuel Henrique O. Pinto

