---
kind: article
created_at: 2017-04-28
title: Palestra de Richard Stallman em Curitiba em 02 de junho
image_file: banner-divulgacao-stallman-2017.png
image_alt: Diulgação Stallman em Curitiba
author: Paulo Henrique de Lima Santana
short_description: |
  Richard Matthew Stallman (RMS), fundador do movimento Software Livre, do Projeto GNU, e da Free Software Foundation (FSF), vem a Curitiba no dia 02 de junho de 2017 para fazer a palestra:
---

[Richard Matthew Stallman (RMS)](https://stallman.org), fundador do movimento Software Livre, do [Projeto GNU](https://www.gnu.org), e da [Free Software Foundation (FSF)](http://www.fsf.org), vem a Curitiba no dia 02 de junho de 2017 para fazer a palestra:

***Free Software and Your Freedom.***

Descrição: The Free Software Movement campaigns for computer users' freedom to cooperate and control their own computing. The Free Software Movement developed the GNU operating system, typically used together with the kernel Linux, specifically to make these freedoms possible.

Você pode contribuir com a nossa campanha de financiamento coletivo doando qualquer valor para ajudar a alugar uma cabine de tradução e contratar um tradutor, assim as pessoas que não entendem inglês poderão assistir a palestra com tradução simultânea.

<http://rms.curitibalivre.org.br/financiamento-coletivo.shtml>

Mais informações e inscrição gratuita:

<http://rms.curitibalivre.org.br>

**Resumo:**

* Data: 02 de junho
* Horário: 17:00h
* Local: Auditório Prof. Ulysses de Campos do [Setor de Ciências Sociais Aplicadas](http://www.sociaisaplicadas.ufpr.br/portal) da Universidade Federal do Paraná

**Patrocínio:**

* [C3SL](https://www.c3sl.ufpr.br)

**Organização:**

* [Comunidade Curitiba Livre](http://curitibalivre.org.br)
* [CEI - Centro de Estudos de Informática da UFPR](http://cei.inf.ufpr.br)
* [DInf - Departamento de Informática da UFPR](http://www.inf.ufpr.br)


![Stallman em Curitiba](/images/rms-divulgacao-2017.png =600x)
