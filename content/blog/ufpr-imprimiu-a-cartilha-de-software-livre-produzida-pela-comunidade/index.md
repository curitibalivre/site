---
kind: article
created_at: 2014-09-30
title: UFPR imprimiu a cartilha de software livre produzida pela Comunidade   
image_file: banner-cartilha.png
image_alt: Cartilha
author: Paulo Henrique de Lima Santana
short_description: |
  A Assessoria de Comunicação UFPR gentilmente imprimiu mais algumas cartilhas de Software Livre que a Comunidade Curitiba Livre elaborou e distribuiu na Feira de Cursos e Profissões da UFPR em 2013 e 2014.
---

A [Assessoria de Comunicação UFPR](http://www.ufpr.br/portalufpr/imprensa/) gentilmente imprimiu mais algumas cartilhas de Software Livre que a Comunidade Curitiba Livre elaborou e distribuiu na [Feira de Cursos e Profissões da UFPR](http://www.feiradecursos.ufpr.br/) em 2013 e 2014.

Obrigado a UFPR! Agora poderemos distribuir em outros eventos para pessoas que querem conhecer mais sobre o assunto.

![Panfleto](/images/panfletos-sl-ufpr.jpg =300x)

Arquivos para download:

* [Folder - interno em pdf](/arquivos/folder-sl-interno.pdf)
* [Folder - externo em pdf](/arquivos/folder-sl-externo.pdf)
