---
kind: article
created_at: 2014-10-08
title: 2º Dia do Blender Curitiba acontecerá no dia 25 de outubro de 2014  
image_file: banner-2-dia-blender.png
image_alt: Esclarecimento
author: Paulo Henrique de Lima Santana
short_description: |
  No dia 25 de outubro acontece o 2º Dia do Blender Curitiba, seguindo o sucesso que será o dia do Blender Rondonópolis. Serão duas palestras e uma oficina onde o Blender é o assunto principal. O evento é parceiro da semana acadêmica de design da PUC-PR. Se estiver por perto não deixe de participar.
---

No dia 25 de outubro acontece o **2º Dia do Blender Curitiba**, seguindo o sucesso que será o dia do Blender Rondonópolis. Serão duas palestras e uma oficina onde o Blender é o assunto principal. O evento é parceiro da semana acadêmica de design da PUC-PR. Se estiver por perto não deixe de participar.

O evento é voltado para estudantes e profissionais que atuam na área de computação gráfica que se interessam em conhecer, se atualizar e interagir com o software blender e seus usuários da cidade de Curitiba e região.

O Dia do Blender Curitiba acontece no dia 25 de outubro e conta com duas palestras e uma oficina conforme a programação abaixo:

* 9:10 - 10:00 - Palestra com [Elton "Velho Hippie" Ribeiro](https://www.facebook.com/eltonvelhohippie.ribeiro) com o título "Modelagem e planificação de recortes para CNC".
* 10:30 - 11:30 - Palestra sobre modelagem e renderização arquitetônica com o cycles com [Julio Cezar Pires.](http://julio3d.wordpress.com/)
* 13:30 - 16:30 - Oficina de animação em 3D com o Blender nível básico com [Gustavo Mattos](http://gusmattos.blogspot.com.br/).

Para participar é necessário fazer a inscrição com a organização da [Charneira](https://www.facebook.com/charneirapuc) do dia 06/10 até 20/10.

Estamos estudando uma forma de fazer as inscrições a distância para pessoas que não podem comparecer a PUC para fazer a inscrição.

Qualquer dúvida sobre o evento, como participar ou sugestões, basta utilizar este [link](http://www.blender.com.br/index.php?option=com_contact&view=contact&id=1:administracao&catid=46&Itemid=146)

Esperamos todos lá!

Fonte: <http://www.blender.com.br>

