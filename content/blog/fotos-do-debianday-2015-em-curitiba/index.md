---
kind: article
created_at: 2015-08-16
title: Fotos do DebianDay 2015 em Curitiba
image_file: banner-foto-debianday-2015.png
image_alt: Debian Day 2015
author: Paulo Henrique de Lima Santana
short_description: |
  DebianDay realizado no dia 15 de agosto de 2015 no prédio do SEPT UFPR.
---

DebianDay realizado no dia 15 de agosto de 2015 no prédio do SEPT UFPR.

<https://www.flickr.com/photos/curitibalivre/albums/72157657314437622>
