---
kind: article
created_at: 2018-10-01
title: PHP Day Curitiba 2018
image_file: banner-phpday2018.png
image_alt: PHP Day 2018
author: Paulo Henrique de Lima Santana
short_description: |
  Neste sábado (06 de outubro) acontecerá o PHP Day Curitiba 2018 no campus da UniCuritiba.
---

Neste sábado (06/10) acontecerá o PHP Day Curitiba 2018 no campus da UniCuritiba.

A inscrição é gratuita, e entre os palestrantes está confirmada a presença do Rasmus Lerdorf, criador doPHP.

PHP Day Curitiba 2018 é um evento criado para impactar a comunidade local! O intuito é concentrar as mentes mais brilhantes em um só lugar e estimular o crescimento e networking de Curitiba e região metropolitana. Um sábado inteiro sobre boas práticas, melhores ferramentas, novidades da linguagem, tendências, experiência corporativa dos palestrantes, frameworks, bibliotecas e muito network com a união da comunidade local!

Mais informações:

<http://www.phpdaycuritiba.com.br>

![Banner PHP Day](/images/phpday2018.jpg)

