---
kind: article
created_at: 2013-10-01
title: Reunião da comunidade 05/10/13   
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 Está confirmada a reunião da Comunidade Curitiba Livre. 
---

Está confirmada a reunião da Comunidade Curitiba Livre.

Detalhes abaixo:

* Data: 05 de outubro de 2013 (sábado)
* Horário de início: 15:00h
* Local: Sala de reuniões do Fórum do Movimento Ambientalista do Paraná
* Endereço: Rua Gaspar Carrilho Junior, 001- anexo ao Bosque Gutierrez - Bom Retiro.

Pontos de pauta:

* Logomarca do grupo
* Fórum em Defesa|em Prol|de Proteção do Software Livre no Paraná
* Reunião com a SME
* O que mais houver
