---
kind: article
created_at: 2016-11-27
title: Chamada para organização do FLISOL 2017 em Curitiba
image_file: banner-chamada-flisol-2017.png
image_alt: Flisol 2017
author: Paulo Henrique de Lima Santana
short_description: |
  A Comunidade Curitiba Livre está iniciando a organização do FLISOL 2017 em Curitiba, que acontecerá no dia 8 de abril de 2017 e que é um dos maiores eventos de Software Livre do Mundo.
---

A Comunidade Curitiba Livre está iniciando a organização do FLISOL 2017 em Curitiba, que acontecerá no dia 8 de abril de 2017 e que é um dos maiores eventos de Software Livre do Mundo.

Esses eventos são feitos por voluntários, por isso criamos um grupo no telegram para começar a discutir os preparativos.

Se você é de Curitiba ou Região e quer ajudar a organizar o FLISOL 2017 em Curitiba, envie um email para: <voluntarios@curitibalivre.org.br>


