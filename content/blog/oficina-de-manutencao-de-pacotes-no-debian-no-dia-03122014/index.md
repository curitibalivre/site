---
kind: article
created_at: 2014-11-25
title: Oficina de manutenção de pacotes no Debian no dia 03/12/2014 
image_file: banner-oficina-pacotes.png
image_alt: Oficina
author: Paulo Henrique de Lima Santana
short_description: |
  A Comunidade Curitiba Livre e a Rede Livre estão promovendo a oficina de manutenção de pacotes no Debian que acontecerá no dia 03 de dezembro de 2014 a partir das 19:00h na sede da Ethymos.
---

A Comunidade Curitiba Livre e a [Rede Livre](http://www.redelivre.org.br/) estão promovendo a **oficina de manutenção de pacotes no Debian** que acontecerá no dia 03 de dezembro de 2014 a partir das 19:00h na sede da [Ethymos](http://ethymos.com.br/).

O objetivo desse encontro é reunir pessoas de Curitiba interessadas em aprender um pouco mais sobre como contribuir com o [Projeto Debian](http://www.debian.org/) na parte de empacotamento de softwares. A oficina será ministrada pelo [Antonio Terceiro](http://softwarelivre.org/terceiro), desenvolvedor Debian, e será totalmente gratuita. Por ser uma oficina "mão na massa" é necessário trazer o próprio notebook.

## Descrição:

Nesta oficina vamos aprender sobre operações comuns que um mantenedor precisa realizar para colaborar com o Debian: interagir com o sistema de bugs, obter o código fonte de um pacote, realizar modificações necesárias dos pacotes, compilar, testar, e finalmente, fazer o upload.

Durante a oficina, vamos corrigir um ou mais bugs reais no Debian.

## Antonio Terceiro

Desenvolvedor, sócio da [Colivre](http://colivre.coop.br/) onde participou da criação, e participa do desenvolvimento do [Noosfero](http://noosfero.org/). Atualmente trabalha também no projeto de reformulação do Software Público Brasileiro como pesquisador bolsista do [LAPPIS/UNB](http://fga.unb.br/lappis). Também é membro do [projeto Debian](http://www.debian.org/), onde participa do time de Ruby e mantém o projeto de integração contínua do Debian.

## Mais informações:

* Data: 03 de dezembro de 2014
* Horário: 19:00h às 22:00h
* Local: Ethymos
* Endereço: Rua Itupava 1299 - cj 312 - Alto da XV
* Custo: gratuito
* Obs: traga o seu notebook
* Dúvidas: phls@softwarelivre.org
