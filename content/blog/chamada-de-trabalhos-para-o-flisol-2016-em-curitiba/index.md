---
kind: article
created_at: 2016-02-18
title: Chamada de trabalhos para o FLISOL 2016 em Curitiba
image_file: banner-simples-flisol-2016.png
image_alt: Chamada FLISOL 2016
author: Paulo Henrique de Lima Santana
short_description: |
  Está aberta a chamada de trabalhos para o FLISOL 2016 em Curitiba. O evento acontecerá dia 16 de abril de 2016 das 9:00h às 18:00h na PUCPR.
---

Está aberta a chamada de trabalhos para o FLISOL 2016 em Curitiba. O evento acontecerá dia 16 de abril de 2016 das 9:00h às 18:00h na PUCPR.

Serão aceitos palestras e workshops relacionados à Software Livre.

Inscreva sua proposta até dia 02 de marços pelo site:

<http://papers.curitibalivre.org.br/flisol2016>

Maiores informações sobre o evento:

<http://flisol.curitibalivre.org.br>
