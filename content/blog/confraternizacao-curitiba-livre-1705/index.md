---
kind: article
created_at: 2014-05-16
title: Confraternização Curitiba Livre 17/05    
image_file: banner-confraternizacao.png
image_alt: FTSL
author: Paulo Henrique de Lima Santana
short_description: |
 Dia 17 de maio teremos uma confraternização da Comunidade Curitiba Livre.
---

Dia 17 de maio teremos uma confraternização da Comunidade Curitiba Livre

![Banner PHP Day](/images/banner-confr.png)
