---
kind: article
created_at: 2013-12-03
title: Próxima reunião 07/12/13   
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 A próxima reunião da comunidade Curitiba Livre acontecerá no dia 07 de dezembro de 2013 (sábado) a partir das 14h30min.
---

A próxima reunião da comunidade Curitiba Livre acontecerá no dia 07 de dezembro de 2013 (sábado) a partir das 14h30min.

O local da reunião será Garagem Hacker.

Endereço: Rua Antonio Chella, 432 - Bom Retiro
