---
kind: article
created_at: 2017-01-12
title: Inscrição na #CPBR10 com desconto para Comunidade Curitiba Livre
image_file: banner-inscricao-cpbr10.png
image_alt: Inscrição CPBR10
author: Paulo Henrique de Lima Santana
short_description: |
  A Comunidade Curitiba Livre estará presente na Campus Party Brasil edição especial de 10 anos (#CPBR10) com a atividade:
---

A Comunidade Curitiba Livre estará presente na Campus Party Brasil edição especial de 10 anos (#CPBR10) com a atividade:

<http://campuse.ro/events/CPBR10-Grupos/workshop/comunidade-curitiba-livre>

Use o código **#CURITIBALIVRE** no site da CPBR10 e tenha desconto na inscrição:

<http://campuse.ro/events/campus-party-brasil-2017>


![Banner FLISOL 2017](/images/cpbr10-desconto-curitibalivre.png =600x)

