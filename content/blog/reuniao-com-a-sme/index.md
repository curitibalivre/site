---
kind: article
created_at: 2013-09-27
title: Reunião com a SME  
image_file: banner-reuniao-prefeitura.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 Hoje (27/09/13) participamos de uma reunião com a equipe de Tecnologia da Secretaria Municipal de Educação de Curitiba. 
---

Hoje (27/09/13) participamos de uma reunião com a equipe de Tecnologia da Secretaria Municipal de Educação de Curitiba. O diálogo foi muito bom e vimos que a prefeitura está comprometida com o uso de software livre nas escolas municipais. São 18.000 professores de 184 escolas que precisarão de capacitação em GNU/Linux e a comunidade Curitiba Livre está disposta a ajudar no processo.

Estiveram na reunião representando a comunidade Curitiba Livre:

* Paulo Santana
* Antonio Marques
* Rodrigo Robles

Alguns links:

Descrição dos laboratórios do PROINFO/FNDE/MEC:

<http://www.fnde.gov.br/programas/programa-nacional-de-tecnologia-educacional-proinfo/proinfo-perguntas-frequentes>

Lançamento do Programa Conexão Educacional:

<http://www.curitiba.pr.gov.br/noticias/educacao-municipal-tera-uso-de-tecnologia-da-informacao-ampliado/30480>
