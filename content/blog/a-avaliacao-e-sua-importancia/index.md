---
kind: article
created_at: 2015-05-01
title: A avaliação e sua importância
image_file: banner-avaliacao-e-importancia.png
image_alt: Avaliação
author: Leonardo Rocha
short_description: |
  Em um artigo publicado no tiespecialistas (a importância da pesquisa para o mundo dos negócios) trato da importância da pesquisa ao mundo dos negócios.
---

Em um artigo publicado no tiespecialistas [(a importância da pesquisa para o mundo dos negócios)](http://www.tiespecialistas.com.br/2015/04/importancia-da-pesquisa-para-o-mundo-dos-negocios/) trato da importância da pesquisa ao mundo dos negócios. A essência da reflexão proposta neste artigo vale, também, a importância da avaliação de um evento, qualquer que seja ele. Neste sentido, quando uma equipe propõe uma metodologia para desenvolvimento de tarefas, neste caso, para realização de um evento como o **FLISOL - Festival Latino-Americano de Instalação de Solftware Livre**, ela assume a responsabilidade e o compromisso diante de uma comunidade para o sucesso deste evento.

Tendo isso como premissa, a equipe responsável pela 11ª edição do FLISOL que aconteceu no dia 25/04/2015, durante o planejamento deste evento, propós um elo junto à Comunidade Curitiba Livre para que todos(as) pudessem opinar e sugerir melhorias para as próximas edições. Assim, foi criado um formulário de pesquisa com o auxílio do LimeSurvey para que esta avaliação pudesse ser feita.

Para a edição 2015 do FLISOL tivemos aproximadamente 450 inscrições. Destas, 230 pessoas compareceram ao evento e prestigiaram todas as atividades propostas para o dia, dentre elas: palestras, oficinas e InstallFest.

Agora, pedimos que estes participantes avaliem e digam o que poderia melhorar para as próximas edições. O formulário já foi enviado por e-mail a todos(as) os(as) participantes. Pedimos agora que olhem suas caixas de e-mail, lixo eletrônico, spam, enfim, ajudem os(as) voluntários(as) responsáveis pela 11ª edição do FLISOL e aos demais interessados(as) que na próxima edição abraçarão esta causa, e promoverem uma 12ª edição do FLISOL em 2016 ainda melhor.

Contamos com todos(as) vocês para que isso seja possível.
