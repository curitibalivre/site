---
kind: article
created_at: 2014-06-16
title: Mais palestrantes confirmados     
image_file: banner-semana-2014.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 Veja abaixo mais duas importantes personalidades do Software Livre que atuam no Governo Federal e confirmaram presença no evento.
---

Veja abaixo mais duas importantes personalidades do Software Livre que atuam no Governo Federal e confirmaram presença no evento.

* **Marcos Mazoni** é formado em Administração de Empresas, pós-graduado em Tecnologia da Informação pela Fundação Getúlio Vargas e em Gestão Empresarial pela Universidade Federal do Rio Grande do Sul. Desde 2007 é diretor-presidente do Serviço Federal de Processamento de Dados - Serpro, maior empresa de tecnologia da informação da América latina. Foi presidente da Companhia de Processamento de Dados do Rio Grande do Sul (Procergs), de 1999 a 2002 e presidente da empresa estadual de informática do Paraná (Celepar), de 2003 a 2007. É um dos precursores dos sistemas de informática em softwares livres no Brasil, tendo coordenado o I Fórum Internacional de Software Livre realizado em Porto Alegre em 2000. Na Celepar, foi responsável por inúmeros avanços que mudaram o perfil da Empresa, entre eles, a adoção do Programa de Software Livre, que possibilitou o desenvolvimento de soluções e produtos inovadores para os órgãos de Governo, reconhecidos em nível nacional e internacional.

* **Deivi Lopes Kuhn** é entusiasta de Software Livre. Formado em Ciências Econômicas pela UFRGS e pós-graduado em Administração de Redes Linux pela UFLA. É funcionário do SERPRO, aonde exerce o cargo de Coordenador de Inovação e Software Livre na Coordenação de Ações Governamentais. É secretário-executivo do Comitê de Implementação de Software Livre do Governo Brasileiro.
