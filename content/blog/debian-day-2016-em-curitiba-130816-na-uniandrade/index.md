---
kind: article
created_at: 2016-08-02
title: Debian Day 2016 em Curitiba 13/08/16 na Uniandrade
image_file: banner-debianday-2016.png
image_alt: Debian Day 2016
author: Paulo Henrique de Lima Santana
short_description: |
  A Comunidade Curitiba Livre convida a todos para a partir das 10:00h do dia 13 de agosto de 2016 (sábado) celebrar o aniversário do projeto Debian no Centro Universitário Uniandrade.
---


A Comunidade Curitiba Livre convida a todos(as) para a partir das **10:00h do dia 13 de agosto de 2016 (sábado)** celebrar o aniversário do [projeto Debian](http://debian.org) no [Centro Universitário Uniandrade](http://www.uniandrade.br).

As atividades acontecerão no auditório da Uniandrade, que fica na rua João Scuissiato, nº 1, Santa Quitéria - Curitiba PR.

Contamos com a sua presença!

**Sobre o Debian:**

O Projeto Debian é um grupo mundial de voluntários que se esforçam por produzir uma distribuição de um sistema operacional que é composto inteiramente por Software Livre.

O Debian é amplamente usado no mundo em empresas como Exército brasileiro, Eletronorte, Department of Physics, Harvard - EUA, School of Computer Science and Engineering - Canadá, London Health Sciences Centre - Canadá, entre outros.

**Neste ano vamos colocar a mão na massa e presentear o Debian da seguinte forma:**

* Caça a bus e empacotamento: vamos caçar bugs e aprender a empacotar softwares no Debian. Contamos com a sua colaboração.
* Tradução: vamos traduzir do inglês para português páginas do site oficial, documentos e descrições de pacotes do Debian. Então, se você tem facilidade com o inglês e gosta de escrever é só chegar!
* Elaboração de uma página colaborativa sobre Debian: vamos elaborar uma página com links para textos, tutoriais, dicas, vídeos, notícias, etc, sobre o Debian. Se você gosta de navegar na internet aqui é o seu lugar!
* Publicações nas redes sociais: durante o dia vamos publicar informações, curiosidades, links, etc. Se você gosta de compartilhar nas redes sociais e interagir on-line o seu lugar é aqui!

No início das atividades teremos uma palestra sobre como contribuir com o projeto Debian.

**Quais são os pré-requisitos para participar dos grupos de trabalho?**

* Querer aprender/compartilhar conhecimento sobre o Debian.
* Não precisa ter experiência, esse é um dia de aprendizado!
* Para participar das atividades mão na massa é só se juntar ao grupo e começar a contribuir. Se você quer contribuir mas não tem experiência, não tem problema, o pessoal vai te ajudar e te ensinar como fazer.

Quer participar?! Então, acesse o site abaixo para ver mais informações e se inscrever:

<http://debianday.curitibalivre.org.br>

![Minions](/images/lancamento-wheezy-curitiba.gif =600x)

