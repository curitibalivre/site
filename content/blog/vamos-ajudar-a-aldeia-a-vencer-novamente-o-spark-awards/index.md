---
kind: article
created_at: 2015-10-04
title: Vamos ajudar a Aldeia a vencer novamente o Spark Awards
image_file: banner-aldeia-spark.png
image_alt: Aldeia Spark
author: Paulo Henrique de Lima Santana
short_description: |
  Ano passado a Aldeia Coworking venceu o Spark Awards (o Oscar do empreendedorismo brasileiro) na categoria coworking. Esse ano eles estão concorrendo novamente ao prêmio.
---

Ano passado a [Aldeia Coworking](https://aldeia.cc/) venceu o [Spark Awards](http://sparkawards2015.com.br/) (o Oscar do empreendedorismo brasileiro) na categoria coworking. Esse ano eles estão concorrendo novamente ao prêmio.

Como a Aldeia tem apoiado os grupos de software livre em Curitiba cedendo os espaços gratuitamente para atividades, nada mais justo do que a gente dar uma força pra eles e votar para que eles ganhem novamente.

 1. Entre nesse site:< http://sparkawards2015.com.br>
 1. Clique em "login com Facebook", ou crie um cadastro novo (nada será publicado em seu nome)
 1. Concorde com os termos e condições (a gente leu, tá tudo ok!)
 1. Vá até a categoria Coworking (é a 4ª da lista)
 1. Em "sua indicação", escreva Aldeia Coworking e em "referência", coloque o nosso site <https://aldeia.cc>

