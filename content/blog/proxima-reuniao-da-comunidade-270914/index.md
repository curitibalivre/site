---
kind: article
created_at: 2014-09-23
title: Próxima reunião da Comunidade 27/09/14     
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
  Iremos realizar a próxima reunião da Comunidade novamente na sede da Ethymos.
---

Iremos realizar a próxima reunião da Comunidade novamente na sede da [Ethymos](http://ethymos.com.br/).

Resumindo:

* Reunião da Comunidade Curitiba Livre
* Data: 27/09 (sábado)
* Horário: 9:30 às 12:00h
* Local: Ethymos
* Endereço: Rua Itupava, 1.299 - sala 312 - Curitiba

Sugestão de pauta:

* Avaliação da 1a Semana de Software Livre de Curitiba
* Eventos de SL em Curitiba em 2015
* Busca por novos participantes na Comunidade
* Discussão sobre a logo da Comunidade
* Outros assuntos que surgirem
