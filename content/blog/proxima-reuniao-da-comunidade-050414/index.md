---
kind: article
created_at: 2014-03-28
title: Próxima reunião da comunidade 05/04/14   
image_file: banner-reuniao-curitiba-livre.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 O pessoal da Aldeia Coworking cedeu uma sala para realizarmos a nossa primeira reunião da Comunidade no ano.
---

O pessoal da [Aldeia Coworking](https://aldeia.cc/) cedeu uma sala para realizarmos a nossa primeira reunião da Comunidade no ano. Como o pessoal da Comunidade Python tem realizado vários Conding Dojo por lá, com a ajuda do Ramiro decidi procurá-los para pedir o espaço, e fui prontamente atendido.

Resumindo:

* Reunião da Comunidade Curitiba Livre
* Data: 05/04 (sábado)
* Horário: 14:00h às 18:00h
* Local: Aldeia Coworking
* Endereço: Av. Marechal Deodoro, 262 - 1º andar - Galeria Suissa
* Obs: Se o portão da Galeria estiver fechado pela Av. Mal. Deodoro, é só dar a volta no quarteirão e entrar pela rua José Loureiro.

Sugestão de pauta:

* Organização dos próximos eventos de SL em Curitiba
  * Festival Latino-americano de Instalação de Software Livre
  * Debian Day-
  * Fórum de Tecnologia em Software Livre
  * Software Freedom Day
* Outros assuntos que surgirem

