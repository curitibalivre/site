---
kind: article
created_at: 2017-04-25
title: 2º Encontro Debian Curitiba
image_file: banner-2-encontro-debian.png
image_alt: 2 encontro Debian
author: Paulo Henrique de Lima Santana
short_description: |
  A comunidade Debian de Curitiba realizará o seu 2º encontro no dia 20 de maio de 2017 das 9:00h às 17:00h no auditório do Campus da Praça Osório da Universidade Positivo.
---

A comunidade Debian de Curitiba realizará o seu 2º encontro no dia 20 de maio de 2017 das 9:00h às 17:00h no auditório do Campus da Praça Osório da Universidade Positivo.

Mais informações e inscrição gratuita em:

<http://deb.li/2cwb>

![Banner 2o encontro Debian](/images/2-encontro-debian.png =600x)

