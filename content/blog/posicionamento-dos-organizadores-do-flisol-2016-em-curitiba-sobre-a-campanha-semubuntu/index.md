---
kind: article
created_at: 2016-02-10
title: Posicionamento dos organizadores do FLISOL 2016 em Curitiba sobre a campanha #semUbuntu
image_file: banner-flisol-2016.png
image_alt: Flisol sem Ubuntu
author: Paulo Henrique de Lima Santana
short_description: |
  A comunidade de software livre de Curitiba organiza o Festival Latino-americano de Instalação de Software Livre - FLISOL, desde a sua primeira edição em 2005, e em todos esses anos apenas em 2011 o evento não foi realizado na cidade. Ou seja, de 12 edições, estivemos presentes em 11.
---

### Um pouco da história do FLISOL em Curitiba

A comunidade de software livre de Curitiba organiza o [Festival Latino-americano de Instalação de Software Livre - FLISOL](http://flisol.info), desde a sua primeira edição em 2005, e em todos esses anos apenas em 2011 o evento não foi realizado na cidade. Ou seja, de 12 edições, estivemos presentes em 11.

As primeiras edições do FLISOL em Curitiba sempre tiveram muitos visitantes que levaram seus computadores para instalar uma distribuição completa GNU/Linux.  A maioria desses visitantes tinha receio de instalar sozinho uma distribuição, principalmente por desconhecimento de como fazer um dual-boot. Outros visitantes até já haviam instalado uma distribuição, mas precisavam de ajuda para configurar uma determinada placa como de vídeo, rede, modem, som, etc.

Assim como tínhamos muitos visitantes, haviam também muitos instaladores voluntários dispostos a ajudar. Sempre que um instalador perguntava para os coordenadores do FLISOL qual distro eles deveriam instalar para os visitantes, a resposta era sempre a mesma: "instale aquela distribuição que você tem mais conhecimento e que se sinta mais confortável para usar. Você não é obrigado a instalar uma distro que você não conhece, principalmente para não fazer nenhuma bobagem no computador do visitante".

Como naqueles anos a maioria dos instaladores eram membros do [Grupo de Usuários Debian do Paraná - GUD-BR-PR](http://wiki.debianbrasil.org/GUD/PR), a distribuição mais instalada era o [Debian](http://www.debian.org), ou seja, eles instalavam aquilo que eles eram especialistas. Muitas vezes o pessoal do GUD-BR-PR se sentia desafiado a fazer o Debian rodar em qualquer hardware que aparecia e em várias edições eles ficaram após o horário de encerramento em volta de um computador quebrando a cabeça e procurando soluções para uma determinada placa que insistia em não funcionar.

Em várias edições também apareciam voluntários dispostos a instalar outras distribuições como [Conectiva](https://pt.wikipedia.org/wiki/Conectiva) (que tinha a sede da empresa em Curitiba e indicava funcionários para ajudar no evento), [Slackware](http://www.slackware.com), [Fedora](https://getfedora.org), [Ubuntu](http://www.ubuntu.com), etc. Mas esses instaladores de outras distribuições sempre eram em menor número do que o pessoal do GUD-BR-PR, em alguns casos era apenas um instalador de Slackware por exemplo. Mesmo com algumas distribuições sendo mais amigáveis na instalação e configuração, o pessoal do GUD-BR-PR mantinha o  seu ideal de instalar apenas Debian, seguindo a orientação da coordenação de que "cada instalador tinha a liberdade de instalar a distro da sua preferência".

O que acontecia quando um visitante chegava no FLISOL e pedia para alguém instalar uma distribuição especifica mas que não tinha nenhum instalador disponível (seja porque o instalador estava ocupado ou porque não tinha nenhum presente)? Por exemplo, o visitante pedia para instalar o Slackware porque ele tinha ouvido dizer que era legal. A coordenação explicava que não seria possível mas que ele podia ter uma outra distribuição como o Debian porque tinha instalador disponível, e era explicado pra ele as diferenças e semelhanças entre as distribuições. Se o visitante não quisesse ter instalado o Debian, tudo bem, ele não era atendido e não tinha nada instalado no seu computador. Mas nunca, em hipótese alguma, um instalador de Debian era obrigado a instalar Slackware só porque o visitante queria. E isso servia para qualquer distribuição.

Os anos se passaram, o número de visitantes querendo instalar uma distribuição completa ou querendo configurar alguma placa diminuiu bastante principalmente porque todas as distribuições se tornaram muito mais amigáveis para usuários leigos. Consequentemente o número de instaladores também diminuiu e muitos daqueles membros do GUD-BR-PR deixaram de participar do FLISOL. Outros voluntários apareceram, o número de instaladores de Ubuntu aumentou em relação a outras distribuições como Fedora, Mandriva, etc, mas os instaladores de Debian sempre continuaram sendo maioria. O que nunca mudou foi a premissa de que cada instalador tinha a liberdade de escolher qual distribuição instalar, e que ele poderia ser recusar a instalar outra distribuição caso não quisesse.

### A campanha "FLISOL 2015 sem Ubuntu"

No início do mês de março de 2015 Anahuac de Paula Gil lançou uma [petição on-line](http://listas.softwarelivre.org/pipermail/psl-brasil/2015-March/003805.html) para que o FLISOL não se usasse Ubuntu nas instalações. Essa ação iniciou um longo debate dentro da comunidade brasileira de Software Livre a respeito do tema e que dura até hoje.

Alguns dias após a divulgação da petição, o então coodenador nacional do FLISOL no Brasil Thiago Paixão publicou uma [carta aberta](http://listas.softwarelivre.org/pipermail/psl-brasil/2015-March/003958.html) aos coordenadores e colaboradores do FLISOL Brasil com o título *"FLISOL sem Ubuntu"* por meio do qual recomendava a não instalação da distribuição Ubuntu nas cidades que estavam  organizando o evento. O trecho abaixo explica quais os motivos para essa recomendação:

*"Hoje temos esse cenário ocorrendo em diversas distribuições GNU/Linux, mas é inegável que o exemplo mais emblemático fica a cargo da distro mais popular da atualidade, o Ubuntu da Canonical e suas variantes. É de amplo conhecimento que o Ubuntu vem recheado de drivers e softwares privativos em sua instalação padrão, não orientando e muito menos dando opção de não instalação à seus usuários, gerando uma grande desinformação e dependência, em especial dos usuários mais novos. Sua situação se agrava quando em 2012 foi identificado que, sem um prévio aviso adequado e autorização do usuário, o Ubuntu coletava e vendia dados de seus usuários para empresas como a Amazon, tornando-se a primeira distro a adotar tais práticas invasivas que desrespeitam seus usuários e a tudo que é defendido pelo Movimento de Software Livre. No geral, as políticas empregadas pela Canonical em seus produtos, sempre foram questionáveis do ponto de vista das liberdades e filosofia propostas pelo Software Livre, trazendo total desconforto à comunidade como um todo, trazendo à tona todo esse debate."*

Nesse mesmo texto, o Thiago Paixão deixa bem claro que isso é uma recomendação, e não uma imposição da coordenação nacional, ou seja, cada cidade teria autonomia para decidir o que seria ou não instalado nos computadores dos visitantes conforme pode ser visto no trecho abaixo:

*"Deixo claro na carta que não há uma posição oficial da organização do  FLISOL, por tanto a responsabilidade e decisão cabe a nós coordenadores  regionais. De qualquer forma, enfatizo que a carta como um todo, o  posicionamento e a recomendação são do coordenador do nacional, e não  uma posição oficial do FLISOL."*

Mas mesmo com essa afirmação, houve muita confusão gerada por pessoas que passaram a afirmar que o FLISOL no Brasil havia decidido não instalar Ubuntu em nenhuma cidade.  Essa informação equivocada provocou alguns boicotes de pessoas que se acharam excluídas do evento por utilizarem Ubuntu e de outras que acharam injusta a recomendação.

No dia seguinte a publicação do texto *"FLISOL sem Ubuntu"*, Alexandre Oliva, membro do conselho da [Fundação Software Livre América Latina (FSFLA)](http://www.fsfla.org), publicou [um texto](http://listas.softwarelivre.org/pipermail/psl-brasil/2015-March/003963.html) com o título *"Por um FLISOL Exemplar"* em que recomendava que fossem instalados apenas softwares livres. Segue um trecho do texto:

*"Para um novo usuário, a instalação de uma distribuição 100% Livre pode muito bem funcionar 100%, mas do contrário abrirá caminho para explicar o problema do hardware incompatível com a liberdade. É óbvio que a notícia da incompatibilidade será desapontadora para muitos visitantes, e muitos instaladores podem ficar de coração apertado se não "ajudarem" o visitante a instalar blobs e drivers privativos de liberdade exigidos pelo hardware, ou plugins "necessários" para que distribuidores de obras autorais de entretenimento tomem o controle do computador do visitante. São dilemas sem solução favorável: é preciso decidir entre comunicar que algum software privativo de liberdade é aceitável e até desejável, ou arriscar afastar um usuário ao mostrar a importância da resistência firme ao software privativo de liberdade."*

As discussões a respeito do Ubuntu e a instalação de softwares livres e não livres no FLISOL aconteceram  em diversos  espaços como nas listas do [PSL-Brasil](http://listas.softwarelivre.org/pipermail/psl-brasil) e do [flisol-br](http://listas.softwarelivre.org/pipermail/flisol-br) sempre dividindo opiniões.  Muita gente, muita gente mesmo opinou sobre o assunto, e de várias formas diferentes. Uma busca na internet por "flisol sem ubuntu" irá mostrar diversas mensagens de e-emails, textos, vídeos, podcasts, etc.

As cidades organizadoras do [FLISOL 2015 no Brasil](http://flisol.info/FLISOL2015/Brasil) passaram a ter 3 linhas de ação para escolher:

* Instalar para os visitantes qualquer distribuição incluindo o Ubuntu, mesmo com softwares não livres.
* Aderir a campanha "*FLISOL sem Ubuntu"* e instalar para os visitantes qualquer distribuição, exceto o Ubuntu, mesmo com softwares não livres..
* Aderir a campanha *"FLISOL exemplar"* e instalar para os visitantes apenas as [distribuições 100% livres recomendadas pela FSF](http://www.gnu.org/distros/free-distros.pt-br.html) e apenas softwares livres.

### O FLISOL 2015 em Curitiba

Alguns membros da [Comunidade Curitiba Livre](http://www.curitibalivre.org.br) decidiram assinar a petição que apoiava o *"FLISOL sem Ubuntu"*, aderindo assim a campanha. Provocados por todo esse debate, os membros da organização do [FLISOL 2015 em Curitiba](http://softwarelivre.org/flisol2015-curitiba) obviamente passaram a discutir internamento o assunto para chegar a decisão sobre o que seria feito no evento. Após a troca de e-mails e conversas presenciais, ficou claro que não haveria unanimidade dentro do grupo sobre apoiar a campanha do *"FLISOL sem Ubuntu"* ou do *"FLISOL exemplar"*, mas que todo esse debate havia sido muito importante para chamar a atenção sobre o problema de instalar softwares não livres nos computadores dos visitantes.

Sendo assim,  ficou decidido que mesma orientação das edições anteriores do FLISOL em Curitiba continuaria valendo, ou seja, cabe ao voluntário/instalador decidir qual distribuição ele quer instalar, baseado no seu conhecimento. Se um visitante pedisse para instalar uma determinada distribuição e não houvesse nenhum voluntário disposto a instalar, a coordenação deveria orientar esse visitante a aceitar a instalação de outra distribuição.

Outras questões foram ponto de consenso entre os organizadores. São elas:

* Se um visitante chegasse ao evento pedindo algum tipo de orientação sobre a distribuição Ubuntu e houvesse algum voluntário com conhecimento suficiente para ajudá-lo, o visitante receberia a atenção necessária. Sob nenhuma hipótese o visitante seria hostilizado por procurar ajuda sobre o Ubuntu.
* Todos os voluntários dispostos a ajudar nas instalações seriam bem acolhidos,  independente de qual distribuição estivesse disposto a instalar. Sob nenhuma hipótese o instalador seria hostilizado por querer instalar o Ubuntu.
* Os voluntários deveriam orientar a todos os visitantes sobre os problemas de instalar softwares não livres, especialmente blobs binários necessários para fazer algum hardware funcionar com GNU/Linux.

Durante o FLISOL 2015 em Curitiba foram instaladas 11 distribuições GNU/Linux completas, dividas assim:

* Debian = 07
* Fedora = 01
* Mint = 02
* Gentoo = 01

Como se pode ver, não houve nenhuma instalação do Ubuntu devido a ausência de voluntários dispostos a instalar essa distribuição. Aqueles visitantes que chegaram ao evento pedindo para instalar Ubuntu porque só tinham essa referência de GNU/Linux foram orientados a instalar outra distribuição, o que foi aceito por todos. Nenhum visitante que foi ao FLISOL 2015 em Curitiba para instalar uma distribuição GNU/Linux em seu computador/notebook saiu do evento sem conseguir esse objetivo.

### A campanha "FLISOL 2016 #semUbuntu"

No dia 03 de fevereiro de 2016, o Anahuac lançou a campanha "[FLISOL 2016 #semUbuntu"](http://www.anahuac.eu/flisol-2016-semubuntu) para a edição do [FLISOL deste ano](http://flisol.info/FLISOL2016/Brasil). A repercussão até o momento pode ser visto no [histório da lista flisol-br](http://listas.softwarelivre.org/pipermail/flisol-br/2016-February/date.html).

Os membros da [Comunidade Curitiba Livre](http://www.curitibalivre.org.br) que ano passado assinaram a petição que apoiava o *"FLISOL sem Ubuntu"* (e por consequência apoiaram a campanha) este ano deciram não apoiar a campanha *"FLISOL 2016 #semUbuntu"* por entender que a polêmica gerada ano passado não trará benefícios este ano.

### O FLISOL 2016 em Curitiba

Iniciamos novamente a organização do [FLISOL em Curitiba](http://softwarelivre.org/flisol2016-curitiba) que acontecerá no dia 16 de abril de 2016 nas dependências da PUCPR. Na primeira reunião presencial dos organizadores do [FLISOL 2016 em Curitiba](http://softwarelivre.org/flisol2016-curitiba), realizada no dia 14 de novembro de 2015, chegamos a conclusão que nos próximos meses o debate em torno do *"FLISOL sem Ubuntu"* e do *"FLISOL exemplar"* virá novamente à tona na comunidade brasileira de Software Livre, e por isso deveríamos desde já iniciar a discussão sobre como nos posicionaremos a respeito.

A conclusão que chegamos é que a postura adotada no FLISOL 2015 foi bastante satisfatória e que por isso vamos manter as definições, aprimorando e aprofundando algumas questões. Sendo assim, o [FLISOL 2016 em Curitiba](http://softwarelivre.org/flisol2016-curitiba) informa a todos que agiremos na seguinte forma em relação as instalações:

* Caberá a cada voluntário/instalador decidir qual distribuição ele quer instalar, baseado no seu conhecimento.
* Se um  visitante pedir para instalar uma determinada distribuição e não houver nenhum voluntário com disponibilidade para atender a esta especificidade, a coordenação deverá sugerir a esse visitante a instalação de outra distribuição, explicando a política do evento e oferecendo as outras opções existentes.
* Se  um visitante chegar ao evento pedindo algum tipo de orientação sobre a  distribuição Ubuntu e houver algum voluntário com conhecimento suficiente para ajudá-lo, o visitante receberá a atenção necessária.  Sob nenhuma hipótese o visitante será hostilizado por procurar ajuda sobre o Ubuntu.
* Todos  os voluntários dispostos a ajudar nas instalações serão bem  acolhidos, independente de qual distribuição estiver disposto a instalar. Sob nenhuma hipótese o instalador será hostilizado por querer  instalar o Ubuntu.
* Sempre que um voluntário/instalador achar necessário instalar um software não livre, como por exemplo o Flash Player da Adobe para execução de alguns sites ou blobs binários para o funcionamento de algum hardware, ele deverá explicar ao visitante quais os motivos envolvidos (fabricantes que não tem interesse em desenvolver ou disponibilizar especificações para o desenvolvimento de drivers livres), quais as consequências dessa ação (falta de privacidade, falta de segurança, etc),  e que a partir daquele momento, o computador/notebook não terá apenas softwares livres. Após o visitante se sentir esclarecido sobre esse assunto, o voluntário/instalador deverá perguntar se ele concorda ou não com a instalação de softwares não livres, e apenas dar continuidade a instalação com o consentimento do vistante.
* Se houver algum voluntário/instalador disposto a instalar uma distribuição 100% livre como aquelas recomendadas pela FSF, ele deverá informar ao visitante que existe a possibilidade de algum hardware do computador/notebook não funcionar devido a não inclusão blobs binários. Caberá ao visitante decidir se quer manter a instalação dessa distribuição ou se prefere ter outra instalada e por consequência procurar ajuda de outro voluntário/instalador. Provavelmente a instalação de outra distribuição implicará na instalação de blobs binários, o que deve ser esclarecido ao visitante conforma o tópico anterior.

Em um mundo perfeito, deveríamos instalar apenas softwares livres nos computadores/notebooks dos visitantes que vão ao FLISOL para instalar uma distribuição GNU/Linux, seguindo o próprio nome do evento que é: instalação de software livre. Mas devido a intransigência de fabricantes e desenvolvedores que fornecem seus softwares/drivers apenas em formato fechado, em vários casos nos sentimos compelidos a infelizmente instalar softwares não livres para não deixar os visitantes frustrados se o hardware não funcionar corretamente e/ou se não conseguir acessar um determinado site. 

Mas não queremos instalar indiscriminadamente softwares não livres nos computadores/notebooks dos visitantes sem explicar antes o porquê isso acontece e quais as consequências dessa ação. Seja qual for a distribuição GNU/Linux escolhida para instalar, todos os voluntários/instaladores se comprometerão a mostrar os benefícios mas também os malefícios, caso envolvam a instalação de softwares não livres.

Hoje estamos fazendo um FLISOL "possível", mas no futuro, com a pressão das sociedade sobre os fabricantes de hardware e desenvolvedores de software, queremos ter a chance de fazer um FLISOL ideal.

[Organizadores do FLISOL 2016 em Curitiba](http://softwarelivre.org/flisol2016-curitiba/organizadores).

 


