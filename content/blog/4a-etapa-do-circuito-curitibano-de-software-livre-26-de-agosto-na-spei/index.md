---
kind: article
created_at: 2015-08-21
title: 4ª etapa do Circuito Curitibano de Software Livre - 26 de agosto na SPEI
image_file: banner-circuito-4-spei.png
image_alt: Circuito 4 Spei
author: Paulo Henrique de Lima Santana
short_description: |
  A 4ª etapa do Circuito Curitibano de Software Livre acontecerá no Colégio PEI, no dia 26 de agosto de 2015 das 08:00h às 12:00h, com foco nos estudantes do Ensino Médio Técnico em Informática.
---

A **4ª etapa do Circuito Curitibano de Software Livre** acontecerá no [Colégio PEI](http://spei.br/colegio/cursos/ensino-medio-tecnico-em-informatica-4-anos/), no dia 26 de agosto de 2015 das 08:00h às 12:00h, com foco nos estudantes do Ensino Médio Técnico em Informática.

Serão 5 palestras apresentadas por membros da comunidade de Software Livre de Curitiba sobre os conceitos de [Software Livre](http://www.gnu.org/) e a Comunidade Curitiba Livre, a distribuição [Debian GNU/Linux](http://www.debian.org/), a suíte para escritórios [LibreOffice](https://pt-br.libreoffice.org/), a linguagem de programação [Ruby](https://www.ruby-lang.org/pt/), e o reforço do conceiro de que Software Livre não é sinônimo de software gratuito.

O Circuito Curitibano de Software Livre é totalmente gratuito e nesta 4ª etapa é exclusivo para estudantes, professores e funcionários da SPEI.

Aqueles que quiserem participar do sorteio dos brindes, deverão estar presentes no local no momento do encerramento do evento. Nesta etapa, contamos com o apoio do Nic.br que doou um roteador wi-fi do projeto SIMETBox para sorteio. Também teremos algumas camisetas e outros brindes.

Para saber mais sobre o projeto Circuito Curitibano de Software Livre leia a nossa apresentação e se tiver interesse em levar o evento para a sua Faculdade/Universidade, entre em contato conosco através do email: <contato@curitibalivre.org.br>

![Circuito 4 Spei](/images/logos-circuito-4.png)

