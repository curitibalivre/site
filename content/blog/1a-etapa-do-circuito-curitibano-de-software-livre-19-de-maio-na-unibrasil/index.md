---
kind: article
created_at: 2015-05-14
title: 1ª etapa do Circuito Curitibano de Software Livre - 19 de maio na UniBrasil 
image_file: banner-circuito-1-unibrasil.png
image_alt: Circuito 1 UniBrasil
author: Paulo Henrique de Lima Santana
short_description: |
  A 1ª etapa do Circuito Curitibano de Software Livre acontecerá na UniBrasil Centro Universitário, no dia 19 de maio de 2015 das 19:00h às 22h30min na sala 33.
---

A **1ª etapa do Circuito Curitibano de Software Livre** acontecerá na [UniBrasil Centro Universitário](http://www.unibrasil.com.br/), no dia 19 de maio de 2015 das 19:00h às 22h30min na sala 33.

Serão 4 palestras apresentadas por membros da comunidade de Software Livre de Curitiba sobre os conceitos de [Software Livre](http://www.gnu.org/) e a Comunidade Curitiba Livre, a distribuição  [Debian GNU/Linux](http://www.debian.org/), a linguagem de programação [PHP](http://php.net/) e negócios em FLOSS (Free/Libre/Open Source Software).

O Circuito Curitibano de Software Livre é totalmente gratuito e acontecerá dentro da [Semana Acadêmica de Sistemas de Informação](http://avabsi.unibrasil.com.br/) da UniBrasil, evento oferecido a toda a comunidade acadêmica do UniBrasil e aos egressos dos cursos. A programa completa da Semana Acadêmica pode ser vista aqui.

Aqueles que quiserem participar do sorteio dos brindes, deverão preencher o formulário de inscrição e estar presentes no local no momento do encerramento do evento. Nesta etapa, contamos com o apoio da [Editora Novatec](http://novatec.com.br/) que doou um exemplo do livro [Shell Script Profissional](http://www.novatec.com.br/livros/shellscript/) e do Nic.br que doou um roteador wi-fi do projeto SIMETBox para sorteio. Também teremos algumas camisetas e outros brindes.

Para saber mais sobre o projeto Circuito Curitibano de Software Livre leia a nossa apresentação e se tiver interesse em levar o evento para a sua Faculdade/Universidade, entre em contato conosco através do email: <contato@curitibalivre.org.br>

![Circuito 1 UniBrasil](/images/logos-circuito-1.png)

