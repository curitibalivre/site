---
kind: article
created_at: 2015-10-28
title: Django Girls de Curitiba workshop gratuito de programação para mulheres 
image_file: banner-django-girls.png
image_alt: Workshop Django Girls
author: Paulo Henrique de Lima Santana
short_description: |
  O grupo Django Girls de Curitiba está organizando o Workshop gratuito de programação para mulheres que acontecerá no dia no dia 14 de novembro na Aldeia Coworking.
---

O grupo Django Girls de Curitiba está organizando o [Workshop gratuito de programação para mulheres](https://djangogirls.org/curitiba) que acontecerá no dia no dia 14 de novembro na [Aldeia Coworking](https://aldeia.cc).

A programação contará com as seguintes atividades:

* 9:00 - Café da manhã
* 9:30 - Formação dos times
* 12:00 - Hora do almoço
* 13:00 - Retorno ao workshop
* 15:00 - Coffee break
* 15:30 - Mais programação
* 17:00 - Sorteios
* 18:00 - Encerramento

Mais informações e inscrição:

<https://djangogirls.org/curitiba>
