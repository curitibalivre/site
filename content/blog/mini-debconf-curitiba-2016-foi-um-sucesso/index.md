---
kind: article
created_at: 2016-03-09
title: Mini-DebConf Curitiba 2016 foi um sucesso 
image_file: banner-minidebconf-2016.png
image_alt: MiniDebConf 2016
author: Daniel Lenharo de Souza
short_description: |
  A Mini-DebConf Curitiba 2016 foi um sucesso! Tivemos ao longo de 2 dias de dedicação ao Projeto Debian 20 horas de programação. Foram 85 pessoas presentes aproveitando 12 palestras, 07 Lightning Talks e 05 Workshops.
---

Prezad@s colegas, participantes e envolvido/as,

A [MiniDebConf Curitiba 2016](http://br2016.mini.debconf.org) foi um sucesso! Tivemos ao longo de 2 dias de dedicação ao Projeto Debian 20 horas de programação. Foram 85 pessoas presentes aproveitando 12 palestras, 07 Lightning Talks e 05 Workshops.

A organização vem, com enorme alegria e satisfação, agradecer a importante participação, presença e confiança de cada um@ de vocês.

Agradecemos aos palestrantes por dedicarem seu conhecimento para preparar as apresentações e compartilhar com o público presente.

Agradecemos a cada um(a) dos(as) participantes que acreditaram no evento e compareceram e contribuíram para aproximar mais a comunidade Debian.

Agradecemos à [Aldeia Coworking](https://aldeia.cc) por gentilmente ceder o espaço físico e colaborar para a realização do evento.

Por fim, mas não menos importante, agradecemos A TODA EQUIPE dedicada que trabalhou com afinco manhã, tarde, noite e madrugada, meses antes, semanas antes, dias antes do evento iniciar. Cada membro desta equipe superou limites, superou expectativas, buscando o sorriso no rosto com o exercício da solidariedade na solução de qualquer problema que se apresentasse. Esperamos que, de alguma forma, a Mini-DebConf Curitiba 2016 possa ter colaborado para o seu crescimento pessoa e profissional.

A organização convida a todos os envolvidos neste evento a investirem energia na continuidade do espirito colaborativo e fortalecimento da comunidade Debian e do Software Livre no Brasil e no mundo.

Obrigado!!

![MiniDebConf 2016](/images/minidebconf-2016-foto.jpg =600x)

