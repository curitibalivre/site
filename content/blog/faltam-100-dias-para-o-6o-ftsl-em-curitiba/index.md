---
kind: article
created_at: 2014-06-09
title: Faltam 100 dias para o 6º FTSL em Curitiba     
image_file: banner-semana-2014.png
image_alt: Reunião
author: Paulo Henrique de Lima Santana
short_description: |
 Faltando exatamente 100 dias para o início do 6º Fórum de Tecnologia em Software Livre (FTSL) a organização informa que estão abertas as inscrições gratuitas para o evento.
---

Faltando exatamente 100 dias para o início do **6º Fórum de Tecnologia em Software Livre (FTSL)** a organização informa que estão abertas as inscrições **gratuitas** para o evento.

O **FTSL** é um evento anual organizado pelo SERPRO, UTFPR e Comunidade Curitiba Livre, e este ano o FTSL acontecerá de 18 a 20 de setembro. Considerado o maior e mais importante evento de Software Livre de Curitiba e Região tem como propósito a disseminação de novas tecnologias baseadas em Software Livre, bem como, a troca de experiências com as comunidades, universidades e empresas públicas e privadas, por meio de palestras, painéis/mesas-redondas e oficinas/minicursos.

Para realizar a sua inscrição, acesse:

<http://www.ftsl.org.br/inscricao>

A organização aproveita a oportunidade para lembrar que ainda está aberta a **chamada de trabalhos**. Estudantes de qualquer escola ou universidade, professores e/ou profissionais podem enviar suas propostas relacionadas com Software Livre nas categorias: palestra, oficina/minicurso e encontro de comunidade.

Data limite para submissão: **30 de junho de 2014**.

Para enviar a sua proposta de trabalho, acesse:

<http://www.ftsl.org.br/chamada-de-trabalhos>

Comissão Organizadora

6º Fórum de Tecnologia em Software Livre / 10º Software Freedom Day

18 a 20/09/2014 - Campus da UTFPR

![Banner](/images/banner-ftsl6-100.png)
