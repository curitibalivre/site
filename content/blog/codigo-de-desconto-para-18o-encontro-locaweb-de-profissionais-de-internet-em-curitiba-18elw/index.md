---
kind: article
created_at: 2016-05-11
title: Código de desconto para 18º Encontro Locaweb de Profissionais de Internet em Curitiba #18elw
image_file: banner-18-locaweb.png
image_alt: Locaweb
author: Paulo Henrique de Lima Santana
short_description: |
  A Comunidade Curitiba Livre ganhou um código que dá 50% de desconto na inscrição para a 18ª edição do Encontro Locaweb de Profissionais de Internet que vai acontecer dia 21 de maio em Curitiba.
---

A Comunidade Curitiba Livre ganhou um código que dá **50% de desconto** na inscrição para a [18ª edição do Encontro Locaweb de Profissionais de Internet](http://eventos.locaweb.com.br/18o-encontro-locaweb-curitiba) que vai acontecer dia 21 de maio em Curitiba.

Quem quiser o código, é só enviar um email para: <contato@curitibalivre.org.br>

![Banner Locaweb](/images/locaweb-2016.png)
