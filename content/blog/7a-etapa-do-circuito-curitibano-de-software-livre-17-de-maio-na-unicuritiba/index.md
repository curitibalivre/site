---
kind: article
created_at: 2016-05-11
title: 7ª etapa do Circuito Curitibano de Software Livre - 17 de maio na UniCuritiba 
image_file: banner-circuito-7-unicuritiba.png
image_alt: Circuito 7 Unicuritiba
author: Paulo Henrique de Lima Santana
short_description: |
  A 7ª etapa do Circuito Curitibano de Software Livre acontecerá na UNICURITIBA, no dia 17 de maio de 2016 das 19h0min às 22h30min durante a V Jornada de Atualização em Informática.
---

A **7ª etapa do Circuito Curitibano de Software Livre** acontecerá na [UniCuritiba](http://www.unicuritiba.edu.br), no dia 17 de maio de 2016 das 19h0min às 22h30min durante a V Jornada de Atualização em Informática.

Serão 5 palestras apresentadas por membros da comunidade de Software Livre de Curitiba sobre os conceitos de [Software Livre](http://www.gnu.org) e a [Comunidade Curitiba Livre](http://curitibalivre.org.br), a distribuição [Debian GNU/Linux](http://www.debian.org), o CMS [WordPress](https://wordpress.org), Business Intelligence & Analytics, e negócios em Software Livre.

O **Circuito Curitibano de Software Livre** é totalmente gratuito e nesta 7ª etapa é exclusivo para estudantes, professores e funcionários da UniCuritiba.

Aqueles que quiserem participar do sorteio dos brindes, deverão estar presentes no local no momento do encerramento do evento. Nesta etapa, contamos com o apoio do Nic.br que doou um roteador wi-fi do projeto [SIMETBox](http://simet.nic.br/simetbox.html) e da [ASL.Org](http://softwarelivre.org/asl) que doou bolsas/malas do [FISL](http://fisl.org.br)para sorteio. Também teremos algumas camisetas e outros brindes.

Veja a [programação completa](http://softwarelivre.org/circuito-curitibano/programacao-7a-etapa).

Para saber mais sobre o projeto Circuito Curitibano de Software Livre leia a nossa [apresentação](http://softwarelivre.org/circuito-curitibano/apresentacao) e se tiver interesse em levar o evento para a sua Faculdade/Universidade, entre em contato conosco através do email: <contato@curitibalivre.org.br>

![Circuito 7 UniCuritiba](/images/logos-circuito-7.png)

