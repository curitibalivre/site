# Eventos e encontros passados de Software Livre em Curitiba

## 2018

PHP Day Curitiba 2018

 * 06 de outubro de 2018 - 08:30h
 * Local: [UniCuritiba](http://www.unicuritiba.edu.br)
 * Site: <http://www.phpdaycuritiba.com.br>
 * Organizado pela [Comunidade PHP-PR](https://phppr.org)

38° Meetup WordPress Curitiba

 * Data: 26 de setembro de 2018 - 19:00h
 * Local: [Jupter](https://jupter.co)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/254933886>
 * Organizado pela Comunidade [WordPress Curitiba](http://wpcuritiba.com.br)

React CWB #17

 * Data: 25 de setembro de 2018 - 19:00h
 * Local: Pipefy - Av. João Gualberto, 1740 - Juvevê
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/254580596>
 * Organizado pela Comunidade [React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

SFD 2018 - Software Freedom Day

 * 15 de setembro de 2018 - 09:00h
 * Local: [Jupter](https://www.google.com/maps?f=q&amp;hl=en&amp;q=R.+Gen.+Carneiro,+1031+-+Alto+da+Gl%C3%B3ria,+80050-540,+Curitiba,+br)
 * Site: [http://softwarefreedomday.org](a title="http://softwarefreedomday.org" href="http://softwarefreedomday.org)
 * Organizado por [Comunidade Curitiba Livre](http://curitibalivre.org.br)

SciPyLA 2018 - Conferência Latino Americana de Python Científico

 * 29 de agosto e 1 de setembro de 2018
 * Local: [Unicuritiba](http://www.unicuritiba.edu.br)
 * Site: [http://conf.scipyla.org](http://conf.scipyla.org/)
 * Organizado pelo [Grupo de Usuário de Python do Paraná (GruPyPR)](https://grupypr.github.io)

React CWB #16

 * Data: 28 de agosto de 2018 - 19:00h
 * Local: [IEP](http://iep.org.br)
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/253609326>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

37° Meetup WordPress Curitiba (Especial 3 anos!)

 * Data: 25 de agosto de 2018 - 9:00h
 * Local: [Jupter](https://www.google.com/maps?f=q&amp;hl=en&amp;q=R.+Gen.+Carneiro,+1031+-+Alto+da+Gl%C3%B3ria,+80050-540,+Curitiba,+br)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/253694874>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

React CWB #15

 * Data: 31 de julho de 2018 - 19:00h
 * Local: Pipefy - Av. João Gualberto, 1740 - Juvevê
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/252628717>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

36º Meetup WordPress Curitiba

 * Data: 25 de julho de 2018 - 19:00h
 * Local: [Jupter](https://www.google.com/maps?f=q&amp;hl=en&amp;q=R.+Gen.+Carneiro,+1031+-+Alto+da+Gl%C3%B3ria,+80050-540,+Curitiba,+br)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/252905640>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

React CWB #14

 * Data: 26 de junho de 2018 - 19:00h
 * Local: Pipefy - Av. João Gualberto, 1740 - Juvevê
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/251642231>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

35º Meetup WordPress Curitiba

 * Data: 09 de junho de 2018 - 9:00h
 * Local: [NEX Coworking](http://nexcoworking.com.br/curitiba)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/251759784>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

React CWB #13

 * Data: 29 de maio de 2018 - 19:00h
 * Local: Pipefy - Av. João Gualberto, 1740 - Juvevê
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/250838671>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

34º Meetup WordPress Curitiba

 * Data: 24 de maio de 2018 - 19:00h
 * Local: Pipefy - Av. João Gualberto, 1740 - Juvevê
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/250774702>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

FLISOL 2018 - Festival Latino-americano de Instalação de Software Livre

 * Data: 28 de abril 2018 - 9:00 às 17:00
 * Local: Universidade Positivo - Praça Gen. Osório
 * Site: <http://softwarelivre.org/flisol2018-curitiba>
 * Organizado pela Comunidade Curitiba Livre

33º Meetup WordPress Curitiba

 * Data: 25 de abril de 2018 - 19:00h
 * Local: [CD6 Centro de Desenvolvimento](http://cd6.com.br)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/249952403>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

React CWB #12

 * Data: 24 de abril de 2018 - 19:00h
 * Local: Pipefy - Av. João Gualberto, 1740 - Juvevê
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/249667806/>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

MiniDebConf Curitiba 2018

 * Data: 11 a 14 de abril de 2018
 * Local: Funpar, sala 05 - R. João Negrão, 280 - Centro
 * Site: <https://minidebconf.curitiba.br>
 * Organizado pela Comunidade Curitiba Livre

3º MeetUp PostgreSQL Curitiba

 * Data: 14 de abril de 2018 - 15:00h
 * Local: [Funpar](http://www.funpar.ufpr.br)
 * Site: <https://www.meetup.com/pt-BR/PostgreSQL-Curitiba/events/248059697>
 * Organizado pela Comunidade [PostgreSQL Curitiba](https://www.meetup.com/pt-BR/PostgreSQL-Curitiba)

React CWB #11

 * Data: 28 de março de 2018 - 19:00h
 * Local: Pipefy - Av. João Gualberto, 1740 - Juvevê
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/248729775>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

32º Meetup WordPress Curitiba

 * Data: 17 de março de 2018 - 09:00h
 * Local: [NEX Coworking](http://nexcoworking.com.br/curitiba)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/248032094>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

Encontro Debian Women

 * Data: 10 de março de 2018
 * Local: Pipefy - Av. João Gualberto, 1740 - Juvevê
 * Site: <https://debianwomenbr.github.io>

React CWB #10

 * Data: 27 de fevereiro de 2018 - 19:00h
 * Local: Pipefy - Av. João Gualberto, 1740 - Juvevê
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/246907045/>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

31º Meetup WordPress Curitiba

 * Data: 20 de fevereiro de 2018 - 19:00h
 * Local: SESC Paço da Liberdade
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/247647167>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

Primeiro encontro de Data Science Curitiba

 * Data: 06 de fevereiro de 2018 - 19:00h
 * Local: [Jupter](https://www.google.com/maps?f=q&amp;hl=en&amp;q=R.+Gen.+Carneiro,+1031+-+Alto+da+Gl%C3%B3ria,+80050-540,+Curitiba,+br)
 * Site: <https://www.meetup.com/pt-BR/Data-Science-Curitiba/events/247058399>
 * Organizado pela [Comunidade Data Science Curitiba](https://www.meetup.com/pt-BR/Data-Science-Curitiba)

Elixir Meetup #4

 * Data: 06 de fevereiro de 2018 - 19:00h
 * Local: [Pipefy](https://impacthubcuritiba.com/)
 * Site: <https://www.meetup.com/pt-BR/elixircwb/events/246899141>
 * Organizado pela [Comunidade Elixir User Group Paraná (ELUG-PR)](https://www.meetup.com/elixircwb)

React CWB #9

 * Data: 30 de janeiro de 2018 - 19:00h
 * Local: [Pipefy](https://impacthubcuritiba.com/)
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/245193556>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

30º Meetup WordPress Curitiba

 * Data: 20 de janeiro de 2018 - 13:00h
 * Local: [NEX Coworking](http://nexcoworking.com.br/curitiba)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/246648235>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

Elixir Meetup #3

 * Data: 16 de janeiro de 2018 - 19:00h
 * Local: [Ateliware](http://ateliware.com.br)
 * Site: <https://www.meetup.com/pt-BR/elixircwb/events/246452946>
 * Organizado pela [Comunidade Elixir User Group Paraná (ELUG-PR)](https://www.meetup.com/elixircwb)

## 2017

Meetup PHPPR

 * Data: 12 de dezembro de 2017 - 19:30h
 * Local: [Jupter](https://www.google.com/maps?f=q&amp;hl=en&amp;q=R.+Gen.+Carneiro,+1031+-+Alto+da+Gl%C3%B3ria,+80050-540,+Curitiba,+br)
 * Site: <https://www.meetup.com/pt-BR/PHP-PR/events/241681415>
 * Organizado pela [Comunidade PHPPR](https://phppr.org/)

29º Meetup WordPress Curitiba

 * Data: 05 de dezembro de 2017 - 19:00h
 * Local: [Opera Coworking](http://operaco.com.br)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/244967639>
 * Organizado pela Comunidade WordPress Curitiba

React CWB #8

 * Data: 29 de novembro de 2017 - 19:00h
 * Local: [Pipefy](https://impacthubcuritiba.com/)
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/245193556>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

Functional Programming

 * Data: 22 de novembro de 2017
 * Local: [Ebanx](https://www.ebanx.com/br)
 * Site: <https://www.meetup.com/pt-BR/EBANX-meetups-Functional-Programming/events/245107331>
 * Organizado por Ebanx

28º Meetup WordPress Curitiba

 * Data: 18 de novembro de 2017 - 13:30h
 * Local: [CD6 Centro de Desenvolvimento](http://cd6.com.br)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/244967505>
 * Organizado pela Comunidade WordPress Curitiba

Coding dojo na Olist

 * Data: 16 de novembro de 2017 - 19:00h
 * Local: [Olist](https://olist.com)
 * Site: <https://www.meetup.com/pt-BR/GruPy-PR/events/245108297>
 * Organizado pelo GruPy-PR

Meetup PHPPR

 * Data: 14 de novembro de 2017 - 19:30h
 * Local: [Jupter](https://www.google.com/maps?f=q&amp;hl=en&amp;q=R.+Gen.+Carneiro,+1031+-+Alto+da+Gl%C3%B3ria,+80050-540,+Curitiba,+br)
 * Site: <https://www.meetup.com/pt-BR/PHP-PR/events/241681410>
 * Organizado pela [Comunidade PHPPR](https://phppr.org)

Hacktoberfest CWB

 * Data: 28 de outubro de 2017 - 9:00h
 * Local: [IEP](http://iep.org.br)
 * Site: <https://www.meetup.com/pt-BR/GruPy-PR/events/244348318>
 * Organizado pela Comunidades PyLadies Curitiba e GryPy-PR

React CWB #7

 * Data: 25 de outubro de 2017 - 19:00h
 * Local: [Pipefy](https://impacthubcuritiba.com/)
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/243742161>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

27º Meetup WordPress Curitiba

 * Data: 24 de outubro de 2017 - 19:00h
 * Local: [Opera Coworking](http://operaco.com.br)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/243862562>
 * Organizado pela Comunidade WordPress Curitiba

Meetup PHPPR

 * Data: 10 de outubro de 2017 - 19:30h
 * Local: [Jupter](https://www.google.com/maps?f=q&amp;hl=en&amp;q=R.+Gen.+Carneiro,+1031+-+Alto+da+Gl%C3%B3ria,+80050-540,+Curitiba,+br)
 * Site: <https://www.meetup.com/pt-BR/PHP-PR/events/241681409>
 * Organizado pela [Comunidade PHPPR](https://phppr.org/)

26º Meetup WordPress Curitiba: Global WordPress Translation Day 3

 * Data: 30 de setembro de 2017 - 9:00h
 * Local: [CD6 Centro de Desenvolvimento](http://cd6.com.br)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/242491223>
 * Organizado pela Comunidade WordPress Curitiba

React CWB #6

 * Data: 27 de setembro de 2017 - 19:00h
 * Local: [Pipefy](https://impacthubcuritiba.com/)
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/242989625>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

IX FTSL - Fórum de Tecnologia em Software Livre

 * Data: 27 a 29 de setembro de 2017
 * Local: UTFPR
 * Site: <http://ftsl.org.br>
 * Organizado pelo Serpro e UTFPR

Ruby Edition #1

 * Data: 22 de setembro de 2017
 * Local: [Ebanx](https://www.ebanx.com/br)
 * Site: <https://www.meetup.com/EBANX-Meetups/events/243475214>
 * Organizado por Ebanx

Elixir Meetup #2

 * Data: 21 de setembro de 2017 - 19:00h
 * Local: [Ateliware](http://ateliware.com.br)
 * Site: <https://www.meetup.com/pt-BR/elixircwb/events/242988412>
 * Organizado pela [Comunidade Elixir User Group Paraná (ELUG-PR)](https://www.meetup.com/elixircwb)

SFD 2017 - Software Freedom Day

 * Data: 16 de setembro de 2017
 * Local: [Jupter](http://jupter.co)
 * Site: <http://softwarelivre.org/sfd2017-curitiba>
 * Organizado pela [Comunidade Curitiba Livre](http://curitibalivre.org.br)

Meetup PHPPR

 * Data: 12 de setembro de 2017 - 19:30h
 * Local: [Jupter](https://www.google.com/maps?f=q&amp;hl=en&amp;q=R.+Gen.+Carneiro,+1031+-+Alto+da+Gl%C3%B3ria,+80050-540,+Curitiba,+br)
 * Site: <https://www.meetup.com/pt-BR/PHP-PR/events/241681407>
 * Organizado pela [Comunidade PHPPR](https://phppr.org/)

2º MeetUp PostgreSQL Curitiba

 * Data: 02 de setembro de 2017 - 15:00h
 * Local: [Funpar](http://www.funpar.ufpr.br)
 * Site: <https://www.meetup.com/pt-BR/PostgreSQL-Curitiba/events/242511958>
 * Organizado pela Comunidade [PostgreSQL Curitiba](https://www.meetup.com/pt-BR/PostgreSQL-Curitiba/)

React CWB #5

 * Data: 30 de agosto de 2017 - 19:00h
 * Local: [Pipefy](https://impacthubcuritiba.com/)
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/242390760>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

Dois anos Meetup WordPress Curitiba

 * Data: 26 de agosto de 2017 - 9:00h
 * Local: Universidade Positivo - Praça Gen. Osório
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/242491223>
 * Organizado pela Comunidade WordPress Curitiba

Manhã de palestras Python

 * Data: 26 de agosto de 2017 - 9:00h
 * Local: [IEP](http://iep.org.br/iep)
 * Site: <https://www.meetup.com/GruPy-PR/events/242800375>
 * Organizado pelo GruPy-PR

Elixir Meetup #1

 * Data: 23 de agosto de 2017 - 19:00h
 * Local: [Ateliware](http://ateliware.com.br)
 * Site: <https://www.meetup.com/pt-BR/elixircwb/events/242257679>
 * Organizado pela [Comunidade Elixir User Group Paraná (ELUG-PR)](https://www.meetup.com/elixircwb)

Debian Day 2017

 * Data: 19 de agosto de 2017
 * Local: UTFPR
 * Site: <https://wiki.debian.org/Brasil/Eventos/DebianDayCuritiba2017>
 * Organizado pela Comunidade Curitiba Livre

Meetup PHPPR

 * Data: 08 de agosto de 2017 - 19:30h
 * Local: [Jupter](https://www.google.com/maps?f=q&amp;hl=en&amp;q=R.+Gen.+Carneiro,+1031+-+Alto+da+Gl%C3%B3ria,+80050-540,+Curitiba,+br)
 * Site: <https://www.meetup.com/pt-BR/PHP-PR/events/241653487>
 * Organizado pela [Comunidade PHPPR](https://phppr.org/)

23º Meetup WordPress Curitiba

 * Data: 25 de julho de 2017 - 19:00h
 * Local: [Instituto Próspera](http://www.institutoprospera.com.br)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/241830585>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

React CWB #4

 * Data: 26 de julho de 2017 - 19:00h
 * Local: [Pipefy](https://impacthubcuritiba.com/)
 * Site: <https://www.meetup.com/ReactJS-CWB/events/241340744>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

1° Meetup PHPPR de 2017

 * Data: 11 de julho de 2017 - 19:30h
 * Local: [Jupter](https://www.google.com/maps?f=q&amp;hl=en&amp;q=R.+Gen.+Carneiro,+1031+-+Alto+da+Gl%C3%B3ria,+80050-540,+Curitiba,+br)
 * Site: <https://www.meetup.com/pt-BR/PHP-PR/events/241191953>
 * Organizado pela [Comunidade PHPPR](https://phppr.org/)

23º Meetup WordPress Curitiba

 * Data: 22 de junho de 2017 - 19:00h
 * Local: [Opera Coworking](http://operaco.com.br/)
 * Site: <https://www.meetup.com/wpcuritiba/events/240686820>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

React CWB #3 - Intro ao React

 * Data:21 de junho de 2017 - 19:00h
 * Local: [Impact Hub Curitiba](https://impacthubcuritiba.com/)
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/240181208>
 * Organizado pela [Comunidade React CW](https://www.meetup.com/pt-BR/ReactJS-CWB)

1º PHP Day Curitiba de 2017

 * Data: 10 de junho de 2017 - 9:00h
 * Local: [Aldeia Coworking](http://aldeiaco.com.br/)
 * Site: <https://www.meetup.com/PHP-PR/events/237795933>
 * Organizado pela [Comunidade PHPPR](http://phppr.org)

Palestra Richard Stallman em Curitiba

 * Data: 2 de junho de 2017
 * Local: Auditório do Setor de Ciências Sociais Aplicadas da UFPR
 * Site: <http://rms.curitibalivre.org.br>
 * Organizado pela [Comunidade Curitiba Livre](http://curitibalivre.org.br)

22º Meetup WordPress Curitiba

 * Data: 27 de maio de 2017 - 9:00h
 * Local: [Aldeia Coworking](http://aldeiaco.com.br/)
 * Site: <https://www.meetup.com/wpcuritiba/events/239989881>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

2º Encontro Debian Curitiba

 * Data: 20 de maio 2017 - 9:00 às 17:00
 * Local: [Universidade Positivo - Praça Osósio](http://www.up.edu.br/praca-osorio)
 * Site: <https://wiki.debian.org/DebianEvents/br/2017/EncontroDebianCuritiba>
 * Organizado pela Comunidade Debian Curitiba

PentahoDay 2017

 * Data: 11 e 12 de maio 2017
 * Local: [Universidade Positivo - Sede Ecoville](http://www.up.edu.br/institucional/campus-sede-%E2%80%93-ecoville)
 * Site: <http://www.pentahobrasil.com.br/eventos/pentahoday2017>
 * Organizado pela Comunidade Pentaho

React CWB #2 - Intro ao React

 * Data: 10 de maio de 2017 - 19:00h
 * Local: [Pipefy](https://www.google.com/maps?f=q&amp;hl=en&amp;q=+Av.+Jo%C3%A3o+Gualberto+1740,+3+andar+-+Juvev%C3%AA,+Curitiba+-+PR,+80030-001,+Curitiba,+br)
 * Site: <https://www.meetup.com/ReactJS-CWB/events/239196844>
 * Organizado pela [Comunidade React CWB](https://www.meetup.com/pt-BR/ReactJS-CWB/)

21º Meetup WordPress Curitiba

 * Data: 25 de abril de 2017 - 19:00h
 * Local: [NEX Coworking](http://nexcoworking.com.br/curitiba)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/239103806/>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

React CWB #1 - Intro ao React

 * Data: 12 de abril de 2017 - 19:00h
 * Local: [Impact Hub Curitiba](https://impacthubcuritiba.com/)
 * Site: <https://www.meetup.com/pt-BR/ReactJS-CWB/events/238242209/>
 * Organizado pela [Comunidade React CWB](https://www.meetup.com/pt-BR/ReactJS-CWB/)

FLISOL 2017 - Festival Latino-americano de Instalação de Software Livre

 * Data: 8 de abril 2017 - 9:00 às 17:00
 * Local: [SEPT - UFPR](http://www.sept.ufpr.br)
 * Site: <http://softwarelivre.org/flisol2017-curitiba>
 * Organizado pela Comunidade Curitiba Livre

Encontro GruPy-PR - Grupo de Usuário de Python do Paraná

 * Data: 8 de abril 2017 - 10:00 às 17:00
 * Local: [SEPT - UFPR](http://www.sept.ufpr.br)
 * Site:
 * Organizado pelo GruPy-PR

20º Meetup WordPress Curitiba

 * Data: 22 de março de 2017 - 19:00h - 22:00h
 * Local: [Opera Coworking](http://operaco.com.br/)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/236871393/>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

Encontro PHPPR

 * Data: 18 de março
 * Local: [UTFPR - Universidade Tecnológica Federal do Paraná](http://www.utfpr.edu.br/curitiba/o-campus/pasta2)
 * Site: <http://phppr.org/notifications/phppr-estara-na-minidebconf>
 * Organizado pela [Comunidade PHPPR](http://phppr.org)

MiniDebConf Curitiba 2017

 * Data: 17 a 19 de março de 2017
 * Local: Funpar, sala 05 - R. João Negrão, 280 - Centro
 * Site: <https://www.facebook.com/events/1291506420931048>
 * Organizado pela Comunidade Curitiba Livre

Bate-Papo sobre PostgreSQL

 * Data: 16 de março de 2017
 * Local: Funpar - Rua João Negrão, 280 - Curitiba
 * Site: <https://www.facebook.com/events/1291506420931048>
 * Organizado pela Comunidade [PostgreSQL Curitiba](https://www.meetup.com/pt-BR/PostgreSQL-Curitiba/)

Dia Internacional dos Dados Abertos - CodeAcross - Open Data Day

 * Data: 04 e 05 de de março de 2017
 * Local: [Jupter](https://www.facebook.com/jupter.co)
 * Site: <https://www.facebook.com/events/1779590855695567>
 * Organizado pela Comunidade [Code For Curitiba](https://www.facebook.com/CodeForCuritiba/)

19º Meetup WordPress Curitiba

 * Data: 18 de fevereiro de 2017 - 9:00h - 12:00h
 * Local: [Aldeia Coworking](https://www.aldeiacoworking.com.br/)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/236871362/>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

Meetup JUG Curitiba - Micro-serviçoes com Jhipster

 * Data: 10 de fevereiro de 2017 - 19:30
 * Local: [Instituto de Engenharia do Paraná – IEP](https://www.google.com/maps/place/Rua+Emiliano+Perneta,+174+-+Centro,+Curitiba+-+PR,+Brazil/@-25.4337914,-49.2765428,17z/data=!3m1!4b1!4m5!3m4!1s0x94dce4728111812b:0xd8a69fe5f913f215!8m2!3d-25.4337914!4d-49.2743541?hl=en)
 * Site: <https://www.meetup.com/JUG-Curitiba/events/237091045>
 * Organizado pelo [JUG Curitiba](https://www.meetup.com/pt-BR/JUG-Curitiba/)

18º Meetup WordPress Curitiba

 * Data: 31 de janeiro de 2017 - 19:00h - 22:00h
 * Local: [NEX Coworking](http://nexcoworking.com.br/curitiba)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/236871308/>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

Meetup nodejsCWB[1]

 * Data: 24 de janeiro de 2017 - 19:00h - 22:00h
 * Local: [NEX Coworking](http://nexcoworking.com.br/curitiba)
 * Site: <https://www.meetup.com/Node-js-CWB/events/237016359>
 * Organizado pela [Comunidade Node.js CWB](https://www.meetup.com/pt-BR/Node-js-CWB/)

Workshop gratuito de programação para mulheres - Django Girls de Curitiba 

 * Data: 21 de janeiro de 2017
 * Local: [Aldeia Coworking](https://www.aldeiacoworking.com.br/)
 * Site: <https://djangogirls.org/curitiba>

## 2016

TechLadies

 * Data: 10 de dezembro de 2016 - 9:00 às 17:30
 * Local: Universidade Positivo - Campus Comprido
 * Site: <https://www.facebook.com/events/1314559048574619>
 * Organizado pela [Comunidade TechLadies](http://www.techladies.com.br/)

17º Meetup WordPress Curitiba

 * Data: 3 de dezembro de 2016 - 9:00 às 12:00
 * Local: [Aldeia Coworking](https://www.aldeiacoworking.com.br/)
 * Site: <https://www.meetup.com/wpcuritiba/events/234392297>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

16º Meetup WordPress Curitiba

 * Data: 16 de novembro de 2016 - 19:00 às 22:00
 * Local: [NEX Coworking](http://nexcoworking.com.br/curitiba)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/234392177/>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

Ada Lovelace Day UTFPR 2016

 * Data: 26 de outubro - 16:00 às 20:30
 * Local: UTFPR Câmpus Curitiba
 * Site: <https://www.facebook.com/events/516964878507374>
 * Organizado pelo grupo [Emílias - Armação em Bits](http://emilias.dainf.ct.utfpr.edu.br)

Encontro da Comunidade Debian

 * Data: 29 de outubro de 2016 - 13:00 às 17:00
 * Local: Universidade Positivo Praça Osório
 * Site: <https://www.meetup.com/pt-BR/Comunidade-Curitiba-Livre/events/235059743/>
 * Organizado pela Comunidade Curitiba Livre

Encontro GruPyPR

 * Data: 29 de outubro de 2016 - 13:00 às 17:00
 * Local: Universidade Positivo Praça Osório
 * Site: <http://www.meetup.com/GruPy-PR/events/235109036>
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR)

15º Meetup WordPress Curitiba

 * Data: 22 de outubo de 2016 - 09:00 às 12:00
 * Local: [Aldeia Coworking](https://www.aldeiacoworking.com.br/)
 * Site: <https://www.meetup.com/pt-BR/wpcuritiba/events/234392206/>
 * Organizado pela [Comunidade WordPress Curitiba](http://wpcuritiba.com.br)

Global Day of Coderetreat 2016

 * Data: 22 de outubro - 09:00 às 17:00
 * Local: UTFPR Curitiba
 * Site: <http://www2.dainf.ct.utfpr.edu.br/Members/adolfo/pesquisa/agile-methods/global-day-of-coderetreat-2016-curitiba-brazil>
 * Organizado por Adolfo Neto

Women Techbirthday

 * Data: 22 de outubro - 09:00 às 17:30
 * Local: Universidade Positivo Osório
 * Site: <https://www.facebook.com/events/179327982471295>
 * Organizado pelo grupo [Women Techmakers](http://wtmcuritiba.com.br)

14º Meetup WordPress Curitiba

 * Data: 20 de setembro de 2016
 * Local: [NEX Coworking](http://nexcoworking.com.br/curitiba)
 * Site: <http://www.meetup.com/pt-BR/wpcuritiba/events/233875595>
 * Organizado pela Comunidade WordPress Curitiba

VIII FTSL - Fórum de Tecnologia em Software Livre

 * Data: 30 de agosto a 02 de setembro de 2016
 * Local: UTFPR
 * Site: <http://ftsl.org.br>
 * Organizado pelo Serpro e UTFPR

Um ano Meetup WordPress Curitiba

 * Data: 27 de agosto de 2016
 * Local: [NEX Coworking](http://nexcoworking.com.br/curitiba)
 * Site: <http://www.meetup.com/pt-BR/wpcuritiba/events/232806785>
 * Organizado pela Comunidade WordPress Curitiba

2º Tech Day do GURU-PR

 * Data: 20 de agosto
 * Local: [EBANX](https://www.google.com.br/maps/place/Shopping+It%C3%A1lia+-+R.+Mal.+Deodoro,+630+-+Centro,+Curitiba+-+PR/@-25.4303043,-49.266263,17z/data=!3m1!4b1!4m5!3m4!1s0x94dce46ae045174b:0x13d4ed0a5d878fe7!8m2!3d-25.4303043!4d-49.266263)
 * Site: <http://www.gurupr.org/eventos/2-tech-day-do-guru-pr>
 * Organizado pelo pessoal do GURU-PR

Debian Day 2016

 * Data: 13 de agosto de 2016
 * Local: (http://www.uniandrade.br/)[Uniandrade]
 * Site: <http://softwarelivre.org/debianday2016-curitiba>
 * Organizado pela Comunidade Curitiba Livre

12º MeetUp WordPress Curitiba

 * Data: 20 de julho de 2016
 * Local: (http://muvcoworking.com/)[MUV Coworking]
 * Site: <http://www.meetup.com/pt-BR/wpcuritiba/events/232280970>
 * Organizado pela Comunidade WordPress Curitiba

11º MeetUp WordPress Curitiba

 * Data: 25 de junho de 2016
 * Local: [Aldeia Coworking](https://www.aldeiacoworking.com.br/)
 * Site: <http://www.meetup.com/pt-BR/wpcuritiba/events/231889574>
 * Organizado pela Comunidade WordPress Curitiba

10º MeetUp WordPress Curitiba

 * Data: 25 de maio de 2016
 * Local: [NEX Coworking](http://www.nexcoworking.com.br/)
 * Site: <http://www.meetup.com/pt-BR/wpcuritiba/events/230986537>
 * Organizado pela Comunidade WordPress Curitiba

Circuito Curitibano de Software Livre - 7ª etapa

 * Data: 17 de maio de 2016
 * Local: UNICURITIBA
 * Site: <http://softwarelivre.org/circuito-curitibano/programacao-7a-etapa>
 * Organizado pela Comunidade Curitiba Livre

Democracia em Rede

 * Data: 16 a 20 de maio de 2016
 * Local: UFPR - Reitoria
 * Site: <http://democraciaemrede.redelivre.org.br>
 * Organizado pelo Laboratório de Cultura Digital

Scratch Day

 * Data: 14 de maio de 2016
 * Local: UFPR - Centro Politécnico
 * Site: <https://projetoeletrizar.wordpress.com/2016/04/18/oficina-de-scratch-programacao-para-pais-e-filhos-2016>
 * Organizado pelo Projeto Eletrizar

FLISOL 2016 - Festival Latino Americano de Instalação de Software Livre

 * Data: 16 de abril 2016
 * Local: PUCPR - Pontifícia Universidade Católica do Paraná
 * Site: <http://softwarelivre.org/flisol2016-curitiba>
 * Organizado pela Comunidade Curitiba Livre

9º MeetUp WordPress Curitiba

 * Data: 16 de abril de 2016
 * Local: PUCPR - Pontifícia Universidade Católica do Paraná
 * Site: <http://www.meetup.com/wpcuritiba/events/230325717>
 * Organizado pela Comunidade WordPress Curitiba

8º MeetUp WordPress Curitiba

 * Data: 23 de março de 2016
 * Local: [Aldeia Coworking](https://www.aldeiacoworking.com.br/)
 * Site: <http://www.meetup.com/pt-BR/wpcuritiba/events/229049418/?eventId=229049418>
 * Organizado pela Comunidade WordPress Curitiba

Encontro GruPyPR na Olist

 * Data: 09 de março de 2016
 * Local: Olist
 * Site: <https://grupypr.mobilize.io/main/groups/3334/events/3225>
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR)

MiniDebConf Curitiba 2016

 * Data: 05 e 06 de março de 2016
 * Local: [Aldeia Coworking](https://www.aldeiacoworking.com.br/)
 * Site: <http://br2016.mini.debconf.org>
 * Organizado pela Comunidade Curitiba Livre

PgDay Curitiba

 * Data: 03 de março de 2016
 * Local: Celepar
 * Site: <http://www.pgdaycuritiba.pr.gov.br>
 * Organizado pelo pessoal da Celepar

7º MeetUp WordPress Curitiba

 * Data: 20 de fevereiro de 2016
 * Local: [Beta - A Fantástica Casa das Startups](http://www.fantasticacasa.com.br/)
 * Site: <http://www.meetup.com/pt-BR/wpcuritiba/events/228890943/?eventId=228890943>
 * Organizado pela Comunidade WordPress Curitiba

Encontro GruPyPR na Olist

 * Data: 17 de fevereiro de 2016
 * Local: Olist
 * Site: <http://www.meetup.com/pt-BR/GruPy-PR/events/228609371>
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR)

6º MeetUp WordPress Curitiba

 * Data: 27 de janeiro de 2016
 * Local: [NEX Coworking](http://www.nexcoworking.com.br)
 * Site: <http://www.meetup.com/pt/wpcuritiba/events/227496631/?eventId=227496631>
 * Organizado pela Comunidade WordPress Curitiba

 
## 2015

5º MeetUp WordPress Curitiba

 * Data: 12 de dezembro de 2015
 * Local: [Beta - A Fantástica Casa das Startups](http://www.fantasticacasa.com.br)
 * Site: <http://www.meetup.com/pt/wpcuritiba/events/227051282/?eventId=227051282>
 * Organizado pela Comunidade WordPress Curitiba

4º MeetUp WordPress Curitiba

 * Data: 18 de novembro de 2015
 * Local: Aldeia Coworking
 * Site: <http://www.meetup.com/pt/wpcuritiba/events/226755697/?eventId=226755697>
 * Organizado pela Comunidade WordPress Curitiba

Workshop gratuito de programação para mulheres - Django Girls de Curitiba 

 * Data: 14 de novembro de 2015
 * Local: [Aldeia Coworking](https://www.aldeiacoworking.com.br/)
 * Site: <https://djangogirls.org/curitiba>

Encontro GruPyPR

 * Data: 21 de outubro de 2015
 * Local: Garagem Hacker
 * Site: <http://www.meetup.com/pt/GruPy-PR/events/226160059>
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR)

Encontro GruPyPR

 * Data: 07 de outubro de 2015
 * Local: Aldeia Coworking
 * Site: <http://www.meetup.com/pt/GruPy-PR/events/225782884>
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR)

Circuito Curitibano de Software Livre - 6ª etapa

 * Data: 07 de outubro de 2015
 * Local: PUCPR
 * Site: <http://softwarelivre.org/circuito-curitibano/programacao-6a-etapa>
 * Organizado pela Comunidade Curitiba Livre

Aniversário de 30 anos da Free Software Foudation

 * Data: 03 de outubro de 2015
 * Local: Aldeia Coworking
 * Site: <http://fsf30.curitibalivre.org.br>
 * Organizado pela Comunidade Curitiba Livre

3º MeetUp WordPress Curitiba

 * Data: 03 de outubro de 2015
 * Local: Aldeia Coworking
 * Site: <http://www.meetup.com/pt/wpcuritiba/events/225444439>
 * Organizado pela Comunidade WordPress Curitiba

Encontro GruPyPR e PyLadies

 * Data: 23 de setembro de 2015
 * Local: Garagem Hacker
 * Site: <http://www.meetup.com/pt/GruPy-PR/events/225512088>
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR) e PyLadies

SFD 2015 - Software Freedom Day

 * Data: 19 de setembro de 2015
 * Local: FATECPR
 * Site: <http://softwarelivre.org/sfd2015-curitiba>
 * Organizado pela Comunidade Curitiba Livre

VII FTSL - Fórum de Tecnologia em Software Livre

 * Data: 16 e 18 de setembro de 2015
 * Local: UTFPR
 * Site: <http://ftsl.org.br>
 * Organizado pelo Serpro e UTFPR

Encontro GruPyPR e PyLadies

 * Data: 09 de setembro de 2015
 * Local: Aldeia Coworking
 * Site: <http://www.meetup.com/pt/GruPy-PR/events/225094006>
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR) e PyLadies

Mais um MeetUp Curitiba para nossa alegria! (WordPress)

 * Data: 09 de setembro de 2015
 * Local: Aldeia Coworrking
 * Site: <http://www.meetup.com/pt/wpcuritiba/events/224947433>
 * Organizado pela Comunidade WordPress Curitiba

Circuito Curitibano de Software Livre - 5ª etapa

 * Data: 02 de setembro de 2015
 * Local: Opet
 * Site: <http://softwarelivre.org/circuito-curitibano/programacao-5a-etapa>
 * Organizado pela Comunidade Curitiba Livre

Encontro GruPyPR e PyLadies

 * Data: 26 de agosto de 2015
 * Local: Garagem Hacker
 * Site: <http://www.meetup.com/pt/GruPy-PR/events/224632839>
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR) e PyLadies

Circuito Curitibano de Software Livre - 4ª etapa

 * Data: 26 de agosto de 2015
 * Local: Spei (ensino médio técnico)
 * Site: <http://softwarelivre.org/circuito-curitibano/programacao-4a-etapa>
 * Organizado pela Comunidade Curitiba Livre

1º Seminário PHP Curitiba

 * Data: 25 de agosto de 2015
 * Local: Serpro
 * Site: <http://phpparanaconference.blogspot.com.br/2015/08/1-seminario-php-curitiba.html>
 * Organizado pela Comunidade PHP

Debian Day 2015

 * Data: 15 de agosto de 2015
 * Local: SEPT UFPR
 * Site: <http://softwarelivre.org/debianday2015-curitiba>
 * Organizado pela Comunidade Curitiba Livre

Encontro GruPyPR e PyLadies

 * Data: 12 de agosto de 2015
 * Local: Aldeia Coworking
 * Site: <http://www.meetup.com/pt/GruPy-PR/events/224423214>
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR) e PyLadies

1º Tech Day do GURU-PR

 * Data: 08 de agosto de 2015
 * Local: EBANX
 * Site: <http://www.gurupr.org/eventos/1-tech-day-do-guru-pr>
 * Organizado pelo Grupo de Usuários de Ruby do Paraná (GURU-PR)

1º MeetUp WordPress Curitiba

 * Data: 05 de agosto de 2015
 * Local: Aldeia Coworking
 * Site: <http://www.meetup.com/pt/wpcuritiba/events/224130389>
 * Organizado pela Comunidade WordPress Curitiba

Circuito Curitibano de Software Livre - 3ª etapa

 * Data: 05 de agosto de 2015
 * Local: Uniandrade
 * Site: <http://softwarelivre.org/circuito-curitibano/programacao-3a-etapa>
 * Organizado pela Comunidade Curitiba Livre

Encontro GruPyPR

 * Data: 29 de julho de 2015
 * Local: Garagem Hacker
 * Site: <http://www.meetup.com/pt/GruPy-PR/events/224076877>
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR)

Encontro GruPyPR

 * Data: 15 de julho de 2015
 * Local: Aldeia Coworking
 * Site:
 * Organizado pelo Grupo de Usuário de Python do Paraná (GruPyPR)

Circuito Curitibano de Software Livre - 2ª etapa

 * Data: 20 de maio de 2015
 * Local: Faculdades Santa Cruz
 * Site: <http://softwarelivre.org/circuito-curitibano/programacao-2a-etapa>
 * Organizado pela Comunidade Curitiba Livre

Circuito Curitibano de Software Livre - 1ª etapa

 * Data: 19 de maio de 2015
 * Local: UniBrasil
 * Site: <http://softwarelivre.org/circuito-curitibano/programacao-1a-etapa>
 * Organizado pela Comunidade Curitiba Livre

FLISOL 2015 - Festival Latino Americano de Instalação de Software Livre

 * Data: 25 de abril de 2015
 * Local: PUCPR - Pontifícia Universidade Católica do Paraná
 * Site: <http://softwarelivre.org/flisol2015-curitiba>
 * Organizado pela Comunidade Curitiba Livre

 
## 2014
 
Dia do Blender 2014

 * Data: 25 de outubro de 2014
 * Local: PUCPR
 * Site: <http://diadoblender.com.br>
 * Organizado pela Comunidade Blender

SFD 2014 - Software Freedom Day

 * Data: 20 de setembro de 2014
 * Local: UTFPR
 * Site: <http://softwarelivre.org/sfd2014-curitiba>
 * Organizado pela Comunidade Curitiba Livre em parceria com a UTFPR e o Serpro

VI FTSL - Fórum de Tecnologia em Software Livre

 * Data: 18 e 19 de setembro de 2014
 * Local: UTFPR
 * Site: <http://ftsl.org.br/2014>
 * Organizado pela Comunidade Curitiba Livre em parceria com a UTFPR e o Serpro

Debian Day 2014

 * Data: 16 de agosto de 2014
 * Local: UTFPR
 * Site: <http://softwarelivre.org/debianday2014-curitiba>
 * Organizado pela Comunidade Curitiba Livre

<strong><strong>FLISOL 2014 - Festival Latino Americano de Instalação de Software Livre</strong></strong>

 * Data: 26 de abril de 2014
 * Local: PUCPR - Pontifícia Universidade Católica do Paraná
 * Site: <http://softwarelivre.org/flisol2014-curitiba>
 * Organizado pela Comunidade Curitiba Livre

<strong>V Workshop da Comunidade Demoiselle</strong>

 * Data: 24 de abril de 2014
 * Local: Auditório do SERPRO Regional Curitiba
 * (http://www.ur1.ca/gvvl1>

<strong>EFD 2014 - Education Freedom Day</strong>

 * Data: 08 de março de 2014
 * Local: UTFPR - Universidade Tecnológica Federal do Paraná
 * Site: <http://softwarelivre.org/efd2014-curitiba>
 * Organizado pela Comunidade Curitiba Livre em parceria com a Secretaria Municipal de Educação de Curitiba

## 2013

Fórum Permanente em Prol do Software Livre no Paraná

 * Data: 23 de novembro de 2013
 * Local: Fórum Ambientalista do Paraná
 * Site: <http://softwarelivre.org/forum-permanente-parana>
 * Organizado pela Comunidade Curitiba Livre

V FTSL - Fórum de Tecnologia em Software Livre

 * Data: 24 e 25 de outubro de 2013
 * Local: UTFPR
 * Site: <http://www.ftsl.org.br>

SFD 2013 - Software Freedom Day

 * Data: 21 de setembro de 2013
 * Local: FESP
 * Site: <http://softwarelivre.org/sfd2013-curitiba>
 * Organizado pela Comunidade Curitiba Livre

Festa de lançamento do Debian Wheezy

 * Data: 11 de maio de 2013
 * Local: APUFPR
 * Site: <http://curitibalivre.org.br/blog/festa-de-lancamento-do-debian-7-wheezy>
 * Organizado pela Comunidade Curitiba Livre

FLISOL 2013 - Festilval Latino Americano de Instalação de Software Livre

 * Data: 27 de abril de 2013
 * Local: FESP
 * Site: <http://softwarelivre.org/flisol2013-curitiba>

## 2012

Palestra com Richard Stallman na UFPR

 * Data: 13 de dezembro de 2012
 * Local: Centro Politécnico da UFPR
 * Site: <http://curitibalivre.org.br/blog/palestra-stallman-ufpr>

SFD 2012 - Software Freedom Day

 * Data: 15 de setembro de 2012
 * Local: FESP
 * Site: <http://softwarelivre.org/sfd2012-curitiba>

Debian Day 2012

 * Data: 18 de agosto de 2012
 * Local: FESP
 * Site: <http://softwarelivre.org/debianday2012-curitiba>

IV FTSL - Fórum de Tecnologia em Software Livre

 * Data: 23 e 24 de junho de 2012
 * Local: Centro Politécnico da UFPR
 * Site: <http://ftsl.org.br/2012>

WordCamp Curitiba 2012

 * Data: 15 e 16 de junho de 2012
 * Local: FESP
 * Site: <http://2012.curitiba.wordcamp.org>

FLISOL 2012 - Festilval Latino Americano de Instalação de Software Livre

 * Data: 28 de abril de 2012
 * Local: FESP
 * Site: <http://softwarelivre.org/flisol2012-curitiba>

DFD 2012 - Document Freedom Day

 * Data: 28 de março de 2012
 * Local: APUFPR
 * Site: <http://softwarelivre.org/flisol2012-curitiba>

## 2011
SFD 2011 - Software Freedom Day

 * Data: 17 de setembro de 2011
 * Local: APUFPR
 * Site: <http://wiki.softwarefreedomday.org/2011/Brazil/Curitiba/GUD-BR-PR>

## 2010

WordCamp Curitiba 2010

 * Data: 22 e 23 de outubro de 2010
 * Local: FESP
 * Site: <http://2010.curitiba.wordcamp.org>

SFD 2010 - Software Freedom Day

 * Data: 18 de setembro de 2010
 * Local: FESP
 * Site: <http://wiki.softwarefreedomday.org/2010/SouthAmerica/Brazil/Curitiba/GUD-BR-PR>

Debian Day 2010

 * Data: 21 de agosto de 2010
 * Local: FESP
 * Site: <http://wiki.debianbrasil.org/GUD/Curitiba/Eventos/DiaDebian2010>

III FTSL - Fórum de Tecnologia em Software Livre

 * Data: 26 a 28 de maio de 2010
 * Local: Serpro
 * Site: <http://www.cta.softwarelivre.serpro.gov.br/curitiba>

FLISOL 2010 - Festilval Latino Americano de Instalação de Software Livre

 * Data: 02 de abril de 2010
 * Local: Faculdades Santa Cruz
 * Site: <http://www.flisol.net/FLISOL2010/Brasil/Curitiba>

## 2009

SFD 2009 - Software Freedom Day

 * Data: 19 de setembro de 2009
 * Local: FESP

FLISOL 2009 - Festilval Latino Americano de Instalação de Software Livre

 * Data: 25 de abril de 2009
 * Local: Faculdades Santa Cruz
 * Site: <http://www.flisol.net/FLISOL2009/Brasil/Curitiba>

## 2008

II FTSL - Fórum de Tecnologia em Software Livre

 * Data: 10 a 12 de novembro de 2008
 * Local: Serpro
 * Site: <http://www.cta.softwarelivre.serpro.gov.br/2008>

SFD 2008 - Software Freedom Day

 * Data: 20 de setembro de 2008
 * Local: UTFPR

FLISOL 2008 - Festilval Latino Americano de Instalação de Software Livre

 * Data: 26 de abril de 2008
 * Local: Faculdades Santa Cruz
 * Site: <http://www.flisol.net/FLISOL2008/Brasil/Curitiba>

## 2007

I FTSL - Fórum de Tecnologia em Software Livre

 * Data: 19 a 21 de novembro de 2007

Debian Day 2007

 * Data: 18 de agosto de 2007
 * Local: Praça de alimentação do Shopping Crystal (encontro)

FLISOL 2007 - Festilval Latino Americano de Instalação de Software Livre

 * Data: 28 de abril de 2007
 * Local: Colégio Estadual do Paraná
 * Site: <http://www.flisol.net/FLISOL2007/Brasil/Curitiba>

## 2006

Debian Day 2006

 * Data: 19 de agosto de 2006
 * Local: Cenai - Centro Nacional de Aprendizado em Informática

FLISOL 2006 - Festilval Latino Americano de Instalação de Software Livre

 * Data: 25 de março de 2006
 * Local: Colégio Estadual do Paraná
 * Site: <http://www.flisol.net/FLISOL2006/Brasil/Curitiba>

## 2005

Debian Day 2005

 * Data: 13 de agosto de 2005
 * Local: Colégio Estadual do Paraná

FLISOL 2005 - Festilval Latino Americano de Instalação de Software Livre

 * Data: 02 de abril de 2005
 * Local: Colégio Estadual do Paraná

## 2004

Debian Day 2004

 * Data: 21 de agosto de 2004
 * Local: Paranácidade

[Lista de eventos do GUD-PR](https://wiki.debian.org/Brasil/GUD/PR/Eventos)
