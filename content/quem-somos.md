# Comunidade de Software Livre de Curitiba e Região

A **Comunidade Curitiba Livre** é um grupo de estudo que atua em prol do movimento de Software Livre na cidade de Curitiba e Região Metropolitana. Nossas atividades sempre são focadas no incentivo ao uso e desenvolvimento de Software Livre além de promovermos a liberdade do conhecimento e a cultura digital.

É importante ressaltar que não somos uma entidade formalmente criada, ou seja, não somos uma pessoa jurídica. A Comunidade é formada por pessoas físicas que estão em diversas categorias da sociedade como: estudantes, empregados em empresas privadas, funcionários públicos, empresários (donos de empresas de TI que trabalham exclusivamente com Software Livre), e membros de outras organizações sociais.

Entre os profissionais que colaboram com a Comunidade temos por exemplo: professores do ensino médio e do ensino superior, gestores de TI, administradores de redes, administradores de sistemas, desenvolvedores, analistas, especialistas em segurança, especialistas em suporte, especialistas em ensino a distância, etc.

Nosso grupo não conta apenas com representantes das áreas técnicas relacionadas à TI. Nele pode-se encontrar representantes de áreas tão diversas como: administração, contabilidade, design, engenharia, educação, políticas públicas, entre outras.

Consideramos qe a Comunidade Curitiba Livre foi fundada em 11 de maio de 2013 no encontro para comemoração do [lançamento do Debian 7 em Curitiba](http://curitibalivre.org.br/blog/festa-de-lancamento-do-debian-7-wheezy).

Todos são bem-vindos a participar do grupo.

Veja fotos das nossas atividades aqui: <https://www.flickr.com/photos/curitibalivre/albums>
